/*
 * Calcoo: b_loadsave.c
 *
 * Copyright (C) 2001,2002, 2007 Alexei Kaminski
 *
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "const.h"
#include "basic.h"
#include "defaults.h"
#include "b_headers.h"
#include "aux_headers.h"


/* Careful with these strings! 
 * Their maximum possible length is defined below */
#define RPN_TRUE_STRING "RPN=True\n"
#define RPN_FALSE_STRING "RPN=False\n"

#define ENTER_TRADITIONAL_STRING "Enter_Mode=Traditional\n"
#define ENTER_HP28_STRING "Enter_Mode=HP-28\n"

#define STACK_INFINITE_STRING "Stack_Type=Infinite\n"
#define STACK_XYZT_STRING "Stack_Type=XYZT\n"

#define ARC_AUTORELEASE_TRUE_STRING "Arc_Autorelease=True\n"
#define ARC_AUTORELEASE_FALSE_STRING "Arc_Autorelease=False\n"

#define HYP_AUTORELEASE_TRUE_STRING "Hyp_Autorelease=True\n"
#define HYP_AUTORELEASE_FALSE_STRING "Hyp_Autorelease=False\n"

#define DEG_STRING "Angle_Units=deg\n"
#define RAD_STRING "Angle_Units=rad\n"

#define ROUNDING_TRUE_STRING "Rounding=True\n"
#define ROUNDING_FALSE_STRING "Rounding=False\n"

#define ROUNDING_VALUE_STRING "Round_To_Digits=" /* no "\n" ! */

#define TRUNC_ZEROS_TRUE_STRING "Trunc_Trailing_Zeros=True\n"
#define TRUNC_ZEROS_FALSE_STRING "Trunc_Trailing_Zeros=False\n"

#define MAX_SETTINGLINE_LENGTH 32
#define MAX_FILENAME_LENGTH 64

static char settings_filename[MAX_FILENAME_LENGTH];

void load_settings(void)
{
	char s1[MAX_SETTINGLINE_LENGTH], s2[MAX_SETTINGLINE_LENGTH],
		s3[MAX_SETTINGLINE_LENGTH], s4[MAX_SETTINGLINE_LENGTH],
		s5[MAX_SETTINGLINE_LENGTH], s6[MAX_SETTINGLINE_LENGTH],
		s7[MAX_SETTINGLINE_LENGTH], s8[MAX_SETTINGLINE_LENGTH], 
		s9[MAX_SETTINGLINE_LENGTH];
	int len, rounding_value;
	FILE *fp;

	snprintf( settings_filename, sizeof(settings_filename) - 1,
		  "%s/.calcoo", getenv("HOME") );

	strcpy(s1," ");
	strcpy(s2," ");
	strcpy(s3," ");
	strcpy(s4," ");
	strcpy(s5," ");
	strcpy(s6," ");
	strcpy(s7," ");
	strcpy(s8," ");
	strcpy(s9," ");

	fp = fopen(settings_filename, "r");

	if( fp != NULL )
	{	
		fgets(s1, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s1, RPN_TRUE_STRING) == 0){
			set_rpn_mode(TRUE);
		} else if (strcmp(s1, RPN_FALSE_STRING) == 0){
			set_rpn_mode(FALSE);
		} else {			
			set_rpn_mode(DEFAULT_RPN_MODE);
		}

		fgets(s2, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s2, ENTER_TRADITIONAL_STRING) == 0){
			set_enter_mode(ENTER_MODE_TRADITIONAL);
		} else if (strcmp(s2, ENTER_HP28_STRING) == 0){
			set_enter_mode(ENTER_MODE_HP28);
		} else {			
			set_enter_mode(DEFAULT_ENTER_MODE);
		}

		fgets(s3, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s3, STACK_INFINITE_STRING) == 0){
			set_stack_mode(STACK_MODE_INFINITE);
		} else if (strcmp(s3, STACK_XYZT_STRING) == 0){
			set_stack_mode(STACK_MODE_XYZT);
		} else {			
			set_stack_mode(DEFAULT_STACK_MODE);
		}

		fgets(s4, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s4, ARC_AUTORELEASE_TRUE_STRING) == 0){
			set_arc_autorelease(TRUE);
		} else if (strcmp(s4, ARC_AUTORELEASE_FALSE_STRING) == 0){
			set_arc_autorelease(FALSE);
		} else {			
			set_arc_autorelease(DEFAULT_ARC_AUTORELEASE);
		}

		fgets(s5, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s5, HYP_AUTORELEASE_TRUE_STRING) == 0){
			set_hyp_autorelease(TRUE);
		} else if (strcmp(s5, HYP_AUTORELEASE_FALSE_STRING) == 0){
			set_hyp_autorelease(FALSE);
		} else {			
			set_hyp_autorelease(DEFAULT_HYP_AUTORELEASE);
		}

		fgets(s6, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s6, DEG_STRING) == 0){
			set_angle_units(CODE_DEG);
		} else if (strcmp(s6, RAD_STRING) == 0){
			set_angle_units(CODE_RAD);
		} else {			
			set_angle_units(DEFAULT_ANGLE_UNITS);
		}

		fgets(s7, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s7, ROUNDING_TRUE_STRING) == 0){
			len = strlen(ROUNDING_VALUE_STRING);
			fgets(s8, MAX_SETTINGLINE_LENGTH, fp);
			if (strncmp(s8, ROUNDING_VALUE_STRING, len) == 0){
				rounding_value = 0;
				sscanf(s8 + len, "%d", &rounding_value);
				if ((rounding_value > 0) 
				    && (rounding_value <= INPUT_LENGTH) )
			set_rounding_mode(rounding_value);
			} else {			
				set_rounding_mode(-INPUT_LENGTH);
			}
		} else if (strcmp(s7, ROUNDING_FALSE_STRING) == 0){
			len = strlen(ROUNDING_VALUE_STRING);
			fgets(s8, MAX_SETTINGLINE_LENGTH, fp);
			if (strncmp(s8, ROUNDING_VALUE_STRING, len) == 0){
				rounding_value = 0;
				sscanf(s8 + len, "%d", &rounding_value);
				if ((rounding_value > 0) 
				    && (rounding_value <= INPUT_LENGTH) )
			set_rounding_mode(-rounding_value);
			} else {			
				set_rounding_mode(-INPUT_LENGTH);
			}
		} else {			
			set_rounding_mode(-INPUT_LENGTH);
		}

		fgets(s9, MAX_SETTINGLINE_LENGTH, fp);
		if (strcmp(s9, TRUNC_ZEROS_TRUE_STRING) == 0){
			set_trunc_zeros_mode(TRUE);
		} else if (strcmp(s9, TRUNC_ZEROS_FALSE_STRING) == 0){
			set_trunc_zeros_mode(FALSE);
		} else {			
			set_trunc_zeros_mode(DEFAULT_TRUNC_ZEROS_MODE);
		}

		fclose(fp);
	} else {
		set_rpn_mode(DEFAULT_RPN_MODE);
		set_enter_mode(DEFAULT_ENTER_MODE);
		set_stack_mode(DEFAULT_STACK_MODE);
		set_arc_autorelease(DEFAULT_ARC_AUTORELEASE);
		set_hyp_autorelease(DEFAULT_HYP_AUTORELEASE);
		set_angle_units(DEFAULT_ANGLE_UNITS);
		set_rounding_mode(-INPUT_LENGTH);
		set_trunc_zeros_mode(DEFAULT_TRUNC_ZEROS_MODE);
		error_occured("load_settings(): could not open settings file",
			      FALSE);
	}
}

void save_settings(void)
{
	int rounding_value;
	FILE *fp;

	fp = fopen(settings_filename,"w");
	if( fp != NULL ){
		if (get_rpn_mode())
			fprintf(fp,"%s", RPN_TRUE_STRING);
		else
			fprintf(fp,"%s", RPN_FALSE_STRING);
		if (get_enter_mode() == ENTER_MODE_TRADITIONAL)
			fprintf(fp,"%s", ENTER_TRADITIONAL_STRING);
		else
			fprintf(fp,"%s", ENTER_HP28_STRING);
		if (get_stack_mode() == STACK_MODE_INFINITE)
			fprintf(fp,"%s", STACK_INFINITE_STRING);
		else
			fprintf(fp,"%s", STACK_XYZT_STRING);
		if (get_arc_autorelease())
			fprintf(fp,"%s", ARC_AUTORELEASE_TRUE_STRING);
		else
			fprintf(fp,"%s", ARC_AUTORELEASE_FALSE_STRING);
		if (get_hyp_autorelease())
			fprintf(fp,"%s", HYP_AUTORELEASE_TRUE_STRING);
		else
			fprintf(fp,"%s", HYP_AUTORELEASE_FALSE_STRING);
		if (get_angle_units() == CODE_DEG)
			fprintf(fp,"%s", DEG_STRING);
		else
			fprintf(fp,"%s", RAD_STRING);
		rounding_value = get_rounding_mode();
		if (rounding_value > 0) {
			fprintf(fp,"%s", ROUNDING_TRUE_STRING);
			fprintf(fp,"%s", ROUNDING_VALUE_STRING);
			fprintf(fp,"%d\n", rounding_value);
		} else {
			fprintf(fp,"%s", ROUNDING_FALSE_STRING);
			fprintf(fp,"%s", ROUNDING_VALUE_STRING);
			fprintf(fp,"%d\n", -rounding_value);
		}
		if (get_trunc_zeros_mode())
			fprintf(fp,"%s", TRUNC_ZEROS_TRUE_STRING);
		else
			fprintf(fp,"%s", TRUNC_ZEROS_FALSE_STRING);

		fclose(fp);
	} else
		error_occured("save_settings(): could not open settings file",
			      FALSE);
}
