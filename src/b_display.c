/*
 * Calcoo: b_display.c
 *
 * Copyright (C) 2001, 2002, 2005 Alexei Kaminski
 *
 * display creation and
 * functions that facilitate display functionality
 *
 */

#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "codes.h"
#include "body.h"
#include "b_headers.h"
#include "aux_headers.h"
#include "gtkaux_headers.h"

void malloc_displays(void)
{
	int i;

	for (i = 0; i < NUMBER_OF_DISPLAYS; i++){
		body->display[i] = 
			(t_body_display*) malloc(sizeof(t_body_display));
		if (body->display[i] == NULL)
			error_occured("malloc failed", TRUE);
	}

	for (i = 0; i < NUMBER_OF_SIMPLE_DISPLAYS; i++){
		body->simple_display[i] = 
			(t_body_simple_display*) 
			malloc(sizeof(t_body_simple_display));
		if(body->display[i] == NULL)
			error_occured("malloc failed", TRUE);
	}
}

/*-------- Displays for numbers ----------------*/
/* (the main display, memory displays, register displays) */
void create_display(int display_number,
		    int x, int y, int w, int h,
		    int cell_width,
		    int dot_offset_vert,
		    int dot_width,
		    int margin_vert,
		    int margin_horiz,
		    int error_indent,
		    int tick_offset_vert,
		    int tick_offset_horiz,
		    int show_frame, 
		    char ***digit_xpm) 
{
	int i, n, x1, y1;
	int display_exp_indent;
	t_body_display *display;

        /* for the sake of brevity: */
	display = body->display[display_number];

	/* Frame */
	if (show_frame) {
		display->frame = gtk_frame_new(NULL);
		gtk_frame_set_shadow_type(GTK_FRAME(display->frame), 
					  GTK_SHADOW_IN);
		gtk_fixed_put(GTK_FIXED(body->fixer), display->frame, x, y);
		gtk_widget_set_size_request(display->frame, w, h);
		gtk_widget_show(display->frame);
	}

	/* Offset for the digits */
	x1 = x + margin_horiz;
	y1 = y + margin_vert;
	display_exp_indent = cell_width * (INPUT_LENGTH + 1) + dot_width;

	display->x1 = x1;
	display->y1 = y1;
	display->display_exp_indent = display_exp_indent;

	display->digit_xpm = digit_xpm;
	display->cell_width = cell_width;
	display->dot_offset_vert = dot_offset_vert;
	display->dot_width = dot_width;
	display->margin_vert = margin_vert;
	display->margin_horiz = margin_horiz;
	display->error_indent = error_indent;
	display->tick_offset_vert = tick_offset_vert;
	display->tick_offset_horiz = tick_offset_horiz;

	/* Things to show */
	for (i = 0; i < INPUT_LENGTH; i++) 
		for (n = 0; n < BASE; n++) 
			display->int_digits[i][n] = NULL;

	for (i = 0; i < INPUT_LENGTH; i++) 
		for (n = 0; n < BASE; n++) 
			display->frac_digits[i][n] = NULL;

	for (i = 0; i < EXP_INPUT_LENGTH; i++) 
		for (n = 0; n < BASE; n++) 
			display->exp_digits[i][n] = NULL;

	for (i = 0; i < INPUT_LENGTH; i++) 
		display->dot[i] = NULL;

	for (i = 0; i < INPUT_LENGTH; i++) 
		display->minus_sign[i] = NULL;

	for (i = 0; i < INPUT_LENGTH; i++) 
		display->int_ticks[i] = NULL;

	for (i = 0; i < INPUT_LENGTH; i++) 
		create_n_put_pixmap(&display->frac_ticks[i],
				    x1 + cell_width * i + dot_width 
				    + tick_offset_horiz,
				    y1 + tick_offset_vert,
				    digit_xpm[D_TICK]);

	display->exp_minus_sign = NULL;
	display->exp_plus_sign = NULL;
	display->E = NULL;

	display->error_message = NULL;
}

void clear_display(int display_number)
{
	int i,n;
	t_body_display *display;

        /* for the sake of brevity: */
	display = body->display[display_number];

	for (i = 0; i < INPUT_LENGTH; i++) 
		for (n = 0; n < BASE; n++) 
			if ( NULL != display->int_digits[i][n] )
				gtk_widget_hide(display->int_digits[i][n]);

	for (i = 0; i < INPUT_LENGTH; i++) 
		for (n = 0; n < BASE; n++) 
			if ( NULL != display->frac_digits[i][n] )
				gtk_widget_hide(display->frac_digits[i][n]);

	for (i = 0; i < EXP_INPUT_LENGTH; i++) 
		for(n = 0; n < BASE; n++) 
			if ( NULL != display->exp_digits[i][n] )
				gtk_widget_hide(display->exp_digits[i][n]);

	for(i = 0; i < INPUT_LENGTH; i++) 
		if ( NULL != display->dot[i] )
			gtk_widget_hide(display->dot[i]);

	for(i = 0; i < INPUT_LENGTH; i++) 
		if ( NULL != display->minus_sign[i] )
			gtk_widget_hide(display->minus_sign[i]);
	
	for(i = 0; i < INPUT_LENGTH; i++) 
		if ( NULL != display->int_ticks[i] )
			gtk_widget_hide(display->int_ticks[i]);

	for(i = 0; i < INPUT_LENGTH; i++) 
		if ( NULL != display->frac_ticks[i] )
			gtk_widget_hide(display->frac_ticks[i]);

	if ( NULL != display->exp_minus_sign )
		gtk_widget_hide(display->exp_minus_sign);
	if ( NULL != display->exp_plus_sign )
		gtk_widget_hide(display->exp_plus_sign);
	if ( NULL != display->E )
		gtk_widget_hide(display->E);

	if ( NULL != display->error_message )
		gtk_widget_hide(display->error_message);
}

void show_int_digit(int d, int i, int n)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->int_digits[i][n] )
		create_n_put_pixmap(&display->int_digits[i][n],
				    display->x1 
				        + display->cell_width * (i + 1), 
				    display->y1,
				    display->digit_xpm[n]);

	gtk_widget_show(body->display[d]->int_digits[i][code_to_digit(n)]);
}

void show_frac_digit(int d, int i, int n)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->frac_digits[i][n] )
		create_n_put_pixmap(&display->frac_digits[i][n],
				    display->x1 + display->dot_width 
				        + display->cell_width * (i + 1), 
				    display->y1,
				    display->digit_xpm[n]);

	gtk_widget_show(body->display[d]->frac_digits[i][code_to_digit(n)]);
}

void show_exp_digit(int d, int i, int n)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->exp_digits[i][n] )
		create_n_put_pixmap(&display->exp_digits[i][n],
				    display->x1 + display->display_exp_indent
				        + display->cell_width * (i + 2), 
				    display->y1,
				    display->digit_xpm[n]);

	gtk_widget_show(body->display[d]->exp_digits[i][code_to_digit(n)]);
}

void show_dot(int d, int i)
{	
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->dot[i] )
		create_n_put_pixmap(&display->dot[i],
				    display->x1 
				        + display->cell_width * (i + 2), 
				    display->y1 + display->dot_offset_vert,
				    display->digit_xpm[D_DOT]);

	gtk_widget_show(body->display[d]->dot[i]);
}

void show_minus(int d, int i)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->minus_sign[i] )
		create_n_put_pixmap(&display->minus_sign[i],
				    display->x1 + display->cell_width * i, 
				    display->y1,
				    display->digit_xpm[D_MINUS]);

	gtk_widget_show(body->display[d]->minus_sign[i]);
}

void show_int_tick(int d, int i)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->int_ticks[i] )
		create_n_put_pixmap(&display->int_ticks[i],
				    display->x1 + display->cell_width * i 
				        + display->tick_offset_horiz,
				    display->y1 + display->tick_offset_vert,
				    display->digit_xpm[D_TICK]);

	gtk_widget_show(body->display[d]->int_ticks[i]);
}

void show_frac_tick(int d, int i)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->frac_ticks[i] )
		create_n_put_pixmap(&display->frac_ticks[i],
				    display->x1 + display->cell_width * i 
				        + display->dot_width 
				        + display->tick_offset_horiz,
				    display->y1 + display->tick_offset_vert,
				    display->digit_xpm[D_TICK]);

	gtk_widget_show(body->display[d]->frac_ticks[i]);
}

void show_E_and_exp_sign(int d, int exp_sign)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->E )
		create_n_put_pixmap(&display->E, 
				    display->x1 + display->display_exp_indent, 
 				    display->y1,
				    display->digit_xpm[D_E]);

	gtk_widget_show(body->display[d]->E);

	switch (exp_sign) {
	case SIGN_PLUS:  
		if ( NULL == display->exp_plus_sign )
			create_n_put_pixmap(&display->exp_plus_sign, 
					    display->x1 
					        + display->display_exp_indent 
					        + display->cell_width, 
					    display->y1,
					    display->digit_xpm[D_PLUS]);

		gtk_widget_show(body->display[d]->exp_plus_sign);
		break;
	case SIGN_MINUS: 
		if ( NULL == display->exp_minus_sign )
			create_n_put_pixmap(&display->exp_minus_sign, 
					    display->x1 
					        + display->display_exp_indent 
					        + display->cell_width, 
					    display->y1,
					    display->digit_xpm[D_MINUS]);

		gtk_widget_show(body->display[d]->exp_minus_sign);
		break;
	}
}

void show_error_message(int d)
{
	t_body_display *display;
	display = body->display[d];
	if ( NULL == display->error_message )
		create_n_put_pixmap(&display->error_message, 
				    display->x1 + display->error_indent, 
				    display->y1,
				    display->digit_xpm[D_OVERFLOW]);

	gtk_widget_show(body->display[d]->error_message);
	strcpy(body->char_display,"error");
}

/*----------- Simple displays -----------------------*/
/* (operation symbols, parens, register names) */

void create_simple_display(int display_number,
			   int x, int y, int w, int h,
			   int number_of_glyphs,
			   int show_frame, 
			   char ***glyph_xpm)
{
	int i;
	t_body_simple_display *display;

        /* for the sake of brevity: */
	display = body->simple_display[display_number];

	/* Frame */
	if (show_frame) {
		display->frame = gtk_frame_new(NULL);
		gtk_frame_set_shadow_type(GTK_FRAME(display->frame), 
					  GTK_SHADOW_IN);
		gtk_fixed_put(GTK_FIXED(body->fixer), display->frame, x, y);
		gtk_widget_set_size_request(display->frame, w, h);
		gtk_widget_show(display->frame);
	}


  	for (i = 0; i < number_of_glyphs; i++)  
  		create_n_put_pixmap(&display->glyph[i], x, y, glyph_xpm[i]); 
  	for (i = number_of_glyphs; i < SD_G_MAX; i++)  
		display->glyph[i] = NULL;

	display->number_of_glyphs = number_of_glyphs;
		       
  	clear_simple_display(display_number); 
}

void clear_simple_display(int display_number)
{
	int i;
	t_body_simple_display *display;

        /* for the sake of brevity: */
	display = body->simple_display[display_number];

	for (i = 0; i < display->number_of_glyphs; i++) 
		if (display->glyph[i] != NULL)
			gtk_widget_hide(display->glyph[i]);
}

void show_display_glyph(int display_number, int glyph_code)
{
	t_body_simple_display *display;

        /* for the sake of brevity: */
	display = body->simple_display[display_number];

	if (display->glyph[glyph_code] != NULL)
		gtk_widget_show(display->glyph[glyph_code]);
}

/*------------ Character string -------------*/
/* not quite a display, but a kind of similar */

void set_char_display(char *s)
{
	strcpy(body->char_display,s);
}

