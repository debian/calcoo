2007/08/18 Calcoo 1.3.18

	* Fixed:   Legacy GTK+ 1.x function calls removed. Thanks 
	           to Rafal Muzylo for reporting the issue and 
		   providing some of the patches.

2007/07/14 Calcoo 1.3.17

	* Fixed:   unsafe strcpy fixed. Thanks to Steve Kemp for
	           reporting the bug and providing the fix.

	* Fixed:   cleaned up parsing pasted selection.
	
2005/04/27 Calcoo 1.3.16

	* Changed: swithched to GTK+ 2.x. Changes needed for the switch 
	           involve keyboard accelerators, dialog windows 
		   (thanks to Reuben Peterkin for providing the patches), 
		   copy/paste functionality.
	
	* Added:   shortcut for the keypad comma (for German keyboards).
	           Thanks to Andreas Hoppe for posting this problem at
		   de.comp.os.unix.apps.kde and testing the fix.

	* Fixed:   all display digits were initialized at startup, which
	           resulted in huge startup times; now they are
		   initialized on demand, and the startup is faster.
	
2003/07/27 Calcoo 1.3.15

	* Added:   some keyboard shortcuts

2003/07/13 Calcoo 1.3.14

	* Fixed:   addition/subtraction tweaked to have 100.1 - 100 - 0.1
	           equal to zero rather than to 10e-17 and so on. Ditto
	           for log and log10. An analogous hack for the
	           trigonometric functions improved. Thanks to Hinrich
	           Buhr for posting this problem at fido.ger.linux

	* Fixed:   function int round(double) was renamed to
	           int round_double_to_int(double) in order to prevent
		   name conflicts under Mac OS X. Thanks to Mike Block 
		   for informing me about this issue.
	
2002/08/18 Calcoo 1.3.13

	* Fixed:   bug which caused segfaults during pasting when the
	           selection could not be retrieved. Thanks to Vincent
	           Lefevre for discovering this bug
	
2002/07/28 Calcoo 1.3.12

	* Added:   accelerator keys: "BackSpace" for undo, "Delete" for
	           clear_x, "Up"/"Down" for stack scrolling (RPN mode)

	* Added:   mouse middle button now pastes into the main display

	* Added:   info and options windows can now be closed by hitting
	           Escape
	
	* Added:   option to truncate trailing zeros when rounding to
	           a fixed number of decimal places
	
	* Fixed:   processing of the command-line options moved to before
	           the call to gtk_init(). As the result, it is now
		   possible to run calcoo with options -h and -v 
		   even if no window can be opened
	
	* Fixed:   it turned out that changing options using the spacebar
	           rather than the mouse was ignored (GTK bug?). 
		   Workaround enabled.
		   
	* Changed: beautifications of the settings window 

	* Changed: minor code cleanups

2002/07/05 Calcoo 1.3.11

	* Fixed:   the bug in setting the color for the button labels and
	           display glyphs. Now, calcoo DOES take this color from
	           the current GTK+ scheme. The default color is
	           fg[PRELIGHT] for the "default" GTK style, as defined in
	           ~/.gtkrc. This choice of color can be changed in
	           defaults.h. Thanks to Jan Stocker for reporting this
	           bug.
	
2002/06/19 Calcoo 1.3.10

	* Changed: in the RPN mode, the keys to swap registers X and Z,
	           and X and T replaced with the keys to scroll the stack

	* Added:   number input can be started with "e" now (mantissa
	           defaults to 1)

	* Fixed:   the lower bound for rounding is changed from "0" (which
	           makes no sense) to "1" 

2002/06/07 Calcoo 1.3.9

	* Added:   option to round output to N digits

	* Fixed:   bugs in the input interpretation which led to improper
	           stack behavior when the number input was beginning with 
		   "." Thanks to Christian Weisgerber for discovering this
	           bug. 
	
	* Fixed:   a minor bug which in scientific and engineering display
	           modes caused unnecessary rounding of numbers very close
		   to 1
	
2002/06/02 Calcoo 1.3.8

	* Changed: non-integer powers of negative numbers now yield errors
	           instead of weird results

	* Added:   saving the selected angle units

	* Added:   option to preserve original colors of pixmaps 
	           (in defaults.h). By default, they are converted to the
		   foreground color.
		   
	* Changed: pixmaps for deg/rad and fix/sci/eng indicators, some
	           other pixmaps
	
	* Fixed:   bugs in saving "arc autorelease" and "hyp autorelease" 
	           settings

	* Changed: minor code cleanups (mostly in b_settings.c)

	* Added:   window icons for Settings and Info windows.

2002/03/09 Calcoo 1.3.7

	* Changed: error handling. Now all the buttons except for undo
	           and clear_all become grayed out when an error occurs.

	* Fixed:   a bug in the initialization sequence, which could cause
	           weird things displayed in the operation displays in
		   the algebraic mode from the calcoo start until the
		   first operation

	* Fixed:   a minor bug in the mode switching procedure, which 
	           could cause weird things displayed in the operation 
		   displays in the algebraic mode from just after 
		   switching from RPN to algebraic mode until the
		   first operation

	* Changed: Minor code cleanups

2002/02/09 Calcoo 1.3.6

	* Added:   option to choose between infinite and four-element
	           stack in RPN mode
	
	* Added:   option to disable autorelease of arc and hyp buttons

	* Changed: RPN-related options are now grayed out when the
	           algebraic mode is chosen

	* Added:   shortcut Ctrl-q for exit

	* Changed: minor code cleanups
	
2002/01/20 Calcoo 1.3.5

	* Fixed:   cpu->curr_mem was not initialized at startup, which
	           caused crashes on some systems. Thanks to Christian
	           Weisgerber for discovering this bug.
	
2001/12/14 Calcoo 1.3.4

	* Changed: RPN mode: behavior of the <Enter> button
	           Finally calcoo can satisfy all tastes for the
		   behavior of the <Enter> button. Now there is an option
		   that sets its behavior to traditional or HP-28-like.

2001/12/10 Calcoo 1.3.3 (for Debian GNU/Linux only)

	* Changed: RPN mode: behavior of the <Enter> button
	           Now it works like in most HP calculators. (In 1.3.2
		   it was set to work as in HP-28, which was not quite
		   standard).

	* Added:   keyboard shortcuts for parenthesis buttons
	
2001/12/3 Calcoo 1.3.2

	* Fixed:   RPN mode: behavior of the <Enter> button pressed
	           when the number has just been entered

	* Changed: now options and info/help have separate windows

	* Changed: code cleanups

	* Added:   engineering display format (the exponent is 
	           a multiple of 3)

	* Fixed:   incorrect behavior when "=" is pressed before all parens
		   are closed

	* Fixed:   adjacent opening parens are now possible

	* Fixed:   GtkErrors if the options applied are the same as the
	           current ones
	
	* Added:   "Help" tab in the info window

	* Changed: register X is not reset by changing the mode now
	
2001/11/19 Calcoo 1.3.1

	* Changed: body design, button arrangement

	* Fixed:   RPN mode: the bugfix of the previous version introduced
	           another bug - sometimes the stack was pushed twice.

	* Added:   displays for Y, Z, and T registers

	* Changed: the number of registers in RPN mode reduced from
	           infinity to 4 to comply with traditions

	* Changed: rearrangements in processing binary operations, digit
	           keys, and parentheses

	* Added:   more shortcuts
	
2001/11/16 Calcoo 1.3.0

	* Changed: body design, button arrangement

	* Fixed:   RPN mode: in earlier versions entering of a new number
	           did not push the stack. Thanks to Christian Weisgerber
	           for finding this bug.

	* Fixed:   PRN mode: in earlier versions recalling from the memory
	           did not push the stack.

	* Added:   a button to swap X and memory registers

	* Added:   another memory register
	
	* Added:   displays for memory

	* Changed: icons for memory buttons

	* Removed: optimization and other flags from configure.in
	
2001/11/13 Calcoo 1.2.4

	* Fixed:   a nasty bug introduced in version 1.2.2, which 
	           caused crashes when display was getting full

	* Changed: pixmaps for digit "1" (thanks to Daniel Richard G.) 

	* Added:   more keyboard shortcuts (thanks to Daniel Richard G.) 
	
2001/11/11 Calcoo 1.2.3

	* Added:   the state of RPN/algebraic option is now saved in
	           $HOME/.calcoo, and is restored when calcoo is restarted

	* Added:   keyboard shortcuts for basic buttons

	* Added:   help tab in the info window

	* Changed: information about the current state of "enforced exp
	           format" and deg/rad is now stored in "cpu"; as a 
	           result now it is affected by undo/redo

	* Fixed:   tan of 90 degrees now yields overflow instead of a large
	           number 

	* Fixed:   undo/redo in the previous versions did not affect memory -
	           now it does

	* Changed: code cleanups, rearrangements in the cpu structure

	* Changed: the icon for the RPN "Enter" button
	
	* Changed: now 0! yields 1 instead of overflow
	
2001/10/20 Calcoo 1.2.2

	* Changed: defaults moved to default.h

	* Fixed:   handling of binary operation priority in complicated
	           expressions improved

	* Changed: behavior of "copy": decimal dot is now not copied
	           if there is no fractional part
	
	* Changed: behavior of "copy": overflow message is now copied
	
2001/10/16  Calcoo 1.2.1

	* Changed: now switching between RPN/algebraic modes resets registers

	* Changed: zero exponent is not displayed now, even if exponential 
	           format is enforced

	* Changed: evaluation of trigonometric functions tweaked to have 
	           cosine of 90 degrees equal to 0.0 rather than to 10e-17 
	           and so on

	* Added:   man page

	* Fixed:   bug in the evaluation of the factorial of large numbers
	
	* Added:   main window icon

	* Added:   --help and --version command-line options

	* Added:   display tick marks to separate thousands

2001/10/13  Calcoo 1.2.0

	* Added:   RPN (reverse Polish notation) mode

	* Changed: appearance of the "Enforce exponential format" control

	* Changed: bitmaps for digits "5" and "1"
	
2001/07/20  Calcoo 1.1.1

	* Changed: icons for some buttons

	* Added:   frame for DEG/RAD indicator
	
2001/07/13  Calcoo 1.1.0

	* Added:   copy/paste interaction with the X clipboard

	* Added:   parentheses buttons

	* Added:   binary operation priority

	* Removed: M-, MX, M/ buttons

2001/06/20  Calcoo 1.0.1

	* Added:   tooltips for non-obvious buttons

	* Added:   autoconf/automake stuff

	* Other:   minor cosmetic changes

2001/06/15 Calcoo 1.0.0
	
	* Release: the first public release
