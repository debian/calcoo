/*
 * Calcoo: b_layout.c
 *
 * Copyright (C) 2001, 2002, 2005 Alexei Kaminski
 *
 * determines layout of the buttons
 * calls create_display()
 *
 * the sizes and layout of everything are defined here
 */

#include <stdlib.h>
#include <stdio.h>

#include "basic.h"
#include "codes.h"
#include "displays.h"
#include "const.h"
#include "defaults.h"
#include "b_headers.h"
#include "io_headers.h"

#define BUTTON_SIZE 32 /* buttons are square */

#define LEFT_MARGIN 8
#define RIGHT_MARGIN 8
#define TOP_MARGIN 8
#define BOTTOM_MARGIN 8

#define DISPLAY_SIZE_HORIZ 240 
#define DISPLAY_SIZE_VERT BUTTON_SIZE
#define UNDERDISPLAY_SPACING (BUTTON_SIZE / 2 )

/* the buttons are groupped in (rectangular) blocks */

#define INTERBLOCK_SPACING (BUTTON_SIZE / 2)

#define NUMBER_OF_ROWS 5
#define NUMBER_OF_COLS_IN_BLOCK_0 1
#define NUMBER_OF_COLS_IN_BLOCK_1 3
#define NUMBER_OF_COLS_IN_BLOCK_2 3
#define NUMBER_OF_COLS_IN_BLOCK_3 4

/* the offset of a block is the distance from the block's left edge
 * to the left edge of the calculator's body */

#define	OFFSET_OF_BLOCK_0 LEFT_MARGIN

#define	OFFSET_OF_BLOCK_1 ( LEFT_MARGIN \
                          + NUMBER_OF_COLS_IN_BLOCK_0 * BUTTON_SIZE \
	                  + INTERBLOCK_SPACING )

#define	OFFSET_OF_BLOCK_2 ( LEFT_MARGIN \
                          + NUMBER_OF_COLS_IN_BLOCK_0 * BUTTON_SIZE \
	                  + INTERBLOCK_SPACING \
                          + NUMBER_OF_COLS_IN_BLOCK_1 * BUTTON_SIZE \
		          + INTERBLOCK_SPACING )

#define	OFFSET_OF_BLOCK_3 ( LEFT_MARGIN \
                          + NUMBER_OF_COLS_IN_BLOCK_0 * BUTTON_SIZE \
	                  + INTERBLOCK_SPACING \
                          + NUMBER_OF_COLS_IN_BLOCK_1 * BUTTON_SIZE \
	                  + INTERBLOCK_SPACING \
		          + NUMBER_OF_COLS_IN_BLOCK_2 * BUTTON_SIZE \
                          + INTERBLOCK_SPACING )

#define	LEFT_FOR_DISPLAY_VERT ( TOP_MARGIN \
                              + DISPLAY_SIZE_VERT \
                              + UNDERDISPLAY_SPACING )
    
#define	BODY_SIZE_VERT ( LEFT_FOR_DISPLAY_VERT \
		       + BUTTON_SIZE * NUMBER_OF_ROWS \
                       + BOTTOM_MARGIN + INTERBLOCK_SPACING)

#define	BODY_SIZE_HORIZ ( LEFT_MARGIN \
		        + NUMBER_OF_COLS_IN_BLOCK_0 * BUTTON_SIZE \
                        + INTERBLOCK_SPACING \
		        + NUMBER_OF_COLS_IN_BLOCK_1 * BUTTON_SIZE \
                        + INTERBLOCK_SPACING \
                        + NUMBER_OF_COLS_IN_BLOCK_2 * BUTTON_SIZE \
                        + INTERBLOCK_SPACING \
                        + NUMBER_OF_COLS_IN_BLOCK_3 * BUTTON_SIZE \
                        + RIGHT_MARGIN )

#define DISPLAY_CELL_WIDTH 15 
#define DISPLAY_DOT_OFFSET_VERT 17
#define DISPLAY_DOT_WIDTH 5
#define DISPLAY_MARGIN_VERT 7
#define DISPLAY_MARGIN_HORIZ 6
#define DISPLAY_OVERFLOW_INDENT 76
#define DISPLAY_TICK_OFFSET_VERT -3
#define DISPLAY_TICK_OFFSET_HORIZ 12
#define DISPLAY_LEFT_SHIFT OFFSET_OF_BLOCK_2

#define MEM_DISPLAY_LEFT_SHIFT ( OFFSET_OF_BLOCK_3 + BUTTON_SIZE / 2 )
#define MEM_DISPLAY_TOP_SHIFT  ( LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4 \
		                            + INTERBLOCK_SPACING )
#define MEM_DISPLAY_V_STEP 16
#define MEM_DISPLAY_SIZE_HORIZ 112
#define MEM_DISPLAY_SIZE_VERT 16
#define MEM_DISPLAY_CELL_WIDTH 7
#define MEM_DISPLAY_DOT_OFFSET_VERT 0
#define MEM_DISPLAY_DOT_WIDTH 3
#define MEM_DISPLAY_MARGIN_VERT 4
#define MEM_DISPLAY_MARGIN_HORIZ 1
#define MEM_DISPLAY_OVERFLOW_INDENT 40

#define REG_DISPLAY_LEFT_SHIFT (LEFT_MARGIN + 12)

void create_widgets(void)
{
	char **button_xpm[MAX_BUTTON_NUMBER];
	char **op_xpm[NUMBER_OF_OPERATION_DISPLAY_GLYPHS];
	char **deg_rad_xpm[NUMBER_OF_ANGLE_UNIT_GLYPHS];
	char **format_xpm[NUMBER_OF_FORMAT_GLYPHS];
	char **reg_display_label_xpm[NUMBER_OF_REG_DISPLAYS];
	static char **digit_xpm[NUMBER_OF_DISPLAY_GLYPHS];
	static char **ldigit_xpm[NUMBER_OF_DISPLAY_GLYPHS];
	/* static because the display digit widgets are created on demand,
	 * therefore one need a static repository for their pixmaps */

	int i;

	create_main_window(BODY_SIZE_HORIZ, BODY_SIZE_VERT);
    
	assign_pixmaps(button_xpm,
		       digit_xpm, 
		       ldigit_xpm,
		       op_xpm,
		       deg_rad_xpm,
		       format_xpm,
		       reg_display_label_xpm);

/*======= Register displays ==========================*/

	create_display(MAIN_DISPLAY,
		       DISPLAY_LEFT_SHIFT,
		       TOP_MARGIN,
		       DISPLAY_SIZE_HORIZ,
		       DISPLAY_SIZE_VERT,
		       DISPLAY_CELL_WIDTH,
		       DISPLAY_DOT_OFFSET_VERT,
		       DISPLAY_DOT_WIDTH,
		       DISPLAY_MARGIN_VERT,
		       DISPLAY_MARGIN_HORIZ,
		       DISPLAY_OVERFLOW_INDENT,
		       DISPLAY_TICK_OFFSET_VERT,
		       DISPLAY_TICK_OFFSET_HORIZ,
		       TRUE, /* frame */
		       digit_xpm);

	for(i = 0; i < NUMBER_OF_REG_DISPLAYS; i++){
		create_display(REG_DISPLAY_OFFSET + i,
			       REG_DISPLAY_LEFT_SHIFT,
			       TOP_MARGIN + (NUMBER_OF_REG_DISPLAYS - i - 1) 
			                    * MEM_DISPLAY_SIZE_VERT,
			       MEM_DISPLAY_SIZE_HORIZ,
			       MEM_DISPLAY_SIZE_VERT,
			       MEM_DISPLAY_CELL_WIDTH,
			       MEM_DISPLAY_DOT_OFFSET_VERT,
			       MEM_DISPLAY_DOT_WIDTH,
			       MEM_DISPLAY_MARGIN_VERT,
			       MEM_DISPLAY_MARGIN_HORIZ,
			       MEM_DISPLAY_OVERFLOW_INDENT,
			       0,
			       0,
			       TRUE, /* frame */ 
  			       ldigit_xpm); 

		create_simple_display(OPERATION_DISPLAY_OFFSET + i,
				      REG_DISPLAY_LEFT_SHIFT +
				      MEM_DISPLAY_SIZE_HORIZ,
				      TOP_MARGIN + 
				      (NUMBER_OF_REG_DISPLAYS - i - 1) 
				      * MEM_DISPLAY_SIZE_VERT,
				      20, 16,
				      NUMBER_OF_OPERATION_DISPLAY_GLYPHS,
				      TRUE, /* frame */ 
				      op_xpm);

		create_simple_display(DISPLAY_LABEL_DISPLAY_OFFSET + i, 
				      LEFT_MARGIN,
				      TOP_MARGIN + 
				      (NUMBER_OF_REG_DISPLAYS - i - 1) 
				      * MEM_DISPLAY_SIZE_VERT,
				      12, 16,
				      NUMBER_OF_REG_DISPLAYS,
				      FALSE, /* frame */ 
				      reg_display_label_xpm);
		show_display_glyph(DISPLAY_LABEL_DISPLAY_OFFSET + i, i);
	}

/*======= Block 0 =====================================*/

	create_button(CODE_DEG_RAD,
		      OFFSET_OF_BLOCK_0, 
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING, 
		      BUTTON_SIZE, BUTTON_SIZE / 2,
		      button_xpm, &clicked_change_angle_units,
		      "Change angle units");

	create_simple_display(ANGLE_UNIT_DISPLAY_OFFSET,
			      OFFSET_OF_BLOCK_0, 
			      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING 
		              + BUTTON_SIZE / 2, 
			      BUTTON_SIZE, BUTTON_SIZE / 2,
			      NUMBER_OF_ANGLE_UNIT_GLYPHS,
			      TRUE, /* frame */ 
			      deg_rad_xpm);

	create_button(CODE_INFO,
		      OFFSET_OF_BLOCK_0, 
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING 
		      + BUTTON_SIZE, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_info, 
		      "About/Help");

	create_button(CODE_SETTINGS,
		      OFFSET_OF_BLOCK_0, 
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING 
		      + BUTTON_SIZE * 2, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_settings, 
		      "Settings");


	create_copy_button(CODE_COPY,
		      OFFSET_OF_BLOCK_0,  
		      LEFT_FOR_DISPLAY_VERT  + INTERBLOCK_SPACING 
			   + BUTTON_SIZE * 3,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, 
		      NULL);

	create_paste_button(CODE_PASTE,
			    OFFSET_OF_BLOCK_0,  
			    LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING 
			    + BUTTON_SIZE * 4, 
			    BUTTON_SIZE, BUTTON_SIZE,
			    button_xpm,
			    NULL);

/*======= Block 1 =====================================*/
/*------- Block 1 Row 1 -------------------------------*/

	create_button(CODE_SIN, 
		      OFFSET_OF_BLOCK_1,  
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_sin, 
		      NULL);

	create_button(CODE_SQR, 
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_sqr, 
		      NULL);

	create_button(CODE_SQRT,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_sqrt, 
		      NULL);

/*----- Block 1 Row 2 -----------------------------------*/

	create_button(CODE_COS,
		      OFFSET_OF_BLOCK_1,
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_cos, 
		      NULL);

	create_button(CODE_ETOX,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_etox, 
		      NULL);
	
	create_button(CODE_LN,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_ln, 
		      NULL);

/*----- Block 1 Row 3 -------------------------------------*/

	create_button(CODE_TAN,
		      OFFSET_OF_BLOCK_1,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_tan, 
		      NULL);

	create_button(CODE_10TOX,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_10tox, 
		      NULL);

	create_button(CODE_LOG,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_log, 
		      NULL);
	
/*----- Block 1 Row 4 ---------------------------------------*/

	create_toggle_button(CODE_ARC,
			     OFFSET_OF_BLOCK_1,
			     LEFT_FOR_DISPLAY_VERT  + BUTTON_SIZE * 3 
		                            + INTERBLOCK_SPACING,  
			     BUTTON_SIZE, BUTTON_SIZE,
			     button_xpm, &toggled_arc_button,
			     NULL);

	create_button(CODE_POW,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_pow, 
		      NULL);

	create_button(CODE_INVX,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE * 2, 
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_invx, 
		      NULL);
	
/*----- Block 1 Row 5 ---------------------------------------*/

	create_toggle_button(CODE_HYP,
			     OFFSET_OF_BLOCK_1,
			     LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4 
		                            + INTERBLOCK_SPACING,  
			     BUTTON_SIZE, BUTTON_SIZE,
			     button_xpm, &toggled_hyp_button,
			     NULL);
	
	create_button(CODE_PI,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_pi, 
		      NULL);

	create_button(CODE_FACT,
		      OFFSET_OF_BLOCK_1 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_fact, 
		      NULL);

/*===== Block 2 ===========================================*/
/*----- Block 2 Row 1 -------------------------------------*/

    	create_button(CODE_7,
		      OFFSET_OF_BLOCK_2,  
		      LEFT_FOR_DISPLAY_VERT,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_7, 
		      NULL);
    
	create_button(CODE_8,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_8, 
		      NULL);

	create_button(CODE_9,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_9, 
		      NULL);

/*----- Block 2 Row 2 -------------------------------------*/

	create_button(CODE_4,
		      OFFSET_OF_BLOCK_2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_4, 
		      NULL);

	create_button(CODE_5,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT  + BUTTON_SIZE,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_5, 
		      NULL);

	create_button(CODE_6,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm,  &clicked_code_6, 
		      NULL);

/*----- Block 2 Row 3 -------------------------------------*/

	create_button(CODE_1,  
		      OFFSET_OF_BLOCK_2, 
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2, 
		      BUTTON_SIZE, BUTTON_SIZE, 
		      button_xpm, &clicked_code_1, 
		      NULL);

	create_button(CODE_2,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT  + BUTTON_SIZE * 2, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_2, 
		      NULL);

	create_button(CODE_3,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE * 2, 
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_3, 
		      NULL);

/*----- Block 2 Row 4 -------------------------------------*/

	create_button(CODE_0,
		      OFFSET_OF_BLOCK_2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_0, 
		      NULL);

	create_button(CODE_SIGN,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_sign, 
		      NULL);

	create_button(CODE_DOT,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_dot, 
		      NULL);

/*----- Block 2 Row 5 ------------------------------------------*/

	create_button(CODE_EXP,
		      OFFSET_OF_BLOCK_2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4
		                            + INTERBLOCK_SPACING,   
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_exp, 
		      NULL);

	create_button(CODE_EXP_SIGN,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_exp_sign, 
		      NULL);

	create_button(CODE_FORMAT,
		      OFFSET_OF_BLOCK_2 + BUTTON_SIZE * 2, 
		      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING 
		              + BUTTON_SIZE * 4, 
		      BUTTON_SIZE, BUTTON_SIZE / 2,
		      button_xpm, &clicked_change_display_format,
		      "Change display format");

	/* format display */
	create_simple_display(FORMAT_DISPLAY_OFFSET,
			      OFFSET_OF_BLOCK_2 + BUTTON_SIZE * 2, 
			      LEFT_FOR_DISPLAY_VERT + INTERBLOCK_SPACING 
		                      + BUTTON_SIZE * 4 + BUTTON_SIZE / 2, 
			      BUTTON_SIZE, BUTTON_SIZE / 2,
			      NUMBER_OF_FORMAT_GLYPHS,
			      TRUE, /* frame */ 
			      format_xpm);

/*====== Block 3 ===============================================*/
/*------ Block 3 Row 1 -----------------------------------------*/

	create_button(CODE_ADD,
		      OFFSET_OF_BLOCK_3,  
		      LEFT_FOR_DISPLAY_VERT,
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_add, 
		      NULL);

	create_button(CODE_SUB,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT,
		      BUTTON_SIZE, BUTTON_SIZE, 
		      button_xpm, &clicked_code_sub, 
		      NULL);

	create_button(CODE_CLEAR_ALL,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_clear_all, 
		      NULL);

	create_button(CODE_UNDO,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 3,  
		      LEFT_FOR_DISPLAY_VERT,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_undo, 
		      "Undo");

/*----- Block 3 Row 2 -----------------------------------------------*/

	create_button(CODE_MUL,
		      OFFSET_OF_BLOCK_3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_mul, 
		      NULL);

	create_button(CODE_DIV,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_div, 
		      NULL);

	create_button(CODE_EQ,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_eq, 
		      NULL);

	create_button(CODE_ENTER,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_enter, 
		      "Enter");

	create_button(CODE_REDO,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_redo, 
		      "Redo");

/*----- Block 3 Row 3 ----------------------------------------------*/

	create_button(CODE_EXCH_XY,
		      OFFSET_OF_BLOCK_3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_exch_xy, 
		      "Swap X and Y");

	create_button(CODE_LEFT_PAREN,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_left_paren, 
		      NULL);

	create_button(CODE_STACK_DOWN,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_stack_down, 
		      "Scroll stack down");

	create_button(CODE_RIGHT_PAREN,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2, 
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_right_paren, 
		      NULL);

	create_button(CODE_STACK_UP,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 2,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_stack_up, 
		      "Scroll stack up");

	create_button(CODE_CLEAR_X,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 2,
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_clear_x, 
		      "Clear X");

/*----- Block 3 Row 4 ----------------------------------------------*/

	create_button(CODE_X_TO_MEM,
		      OFFSET_OF_BLOCK_3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_x_to_mem, 
		      "STO");

	create_button(CODE_MEM_TO_X,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_mem_to_x, 
		      "RCL");

	create_button(CODE_MEM_PLUS,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 2,
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_mem_plus, 
		      "Add X to memory");

	create_button(CODE_EXCH_XMEM,
		      OFFSET_OF_BLOCK_3 + BUTTON_SIZE * 3,
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 3
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE, BUTTON_SIZE,
		      button_xpm, &clicked_code_exch_xmem, 
		      "Swap X and memory");

/*----- Block 3 Row 5 ----------------------------------------------*/

	create_button(CODE_SWITCH_TO_MEM0,
		      OFFSET_OF_BLOCK_3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE / 2, BUTTON_SIZE / 2,
		      button_xpm, &clicked_code_switch_to_mem0, 
		      "Switch to memory register 0");

	create_button(CODE_SWITCH_TO_MEM1,
		      OFFSET_OF_BLOCK_3,  
		      LEFT_FOR_DISPLAY_VERT + BUTTON_SIZE * 4 
		                            + BUTTON_SIZE / 2 
		                            + INTERBLOCK_SPACING,  
		      BUTTON_SIZE / 2, BUTTON_SIZE /2 ,
		      button_xpm, &clicked_code_switch_to_mem1, 
		      "Switch to memory register 1");


	for(i = 0; i < NUMBER_OF_MEMS; i++)
		create_display(MEM_DISPLAY_OFFSET + i,
			       MEM_DISPLAY_LEFT_SHIFT,
			       MEM_DISPLAY_TOP_SHIFT + 
			       i * MEM_DISPLAY_V_STEP,
			       MEM_DISPLAY_SIZE_HORIZ,
			       MEM_DISPLAY_SIZE_VERT,
			       MEM_DISPLAY_CELL_WIDTH,
			       MEM_DISPLAY_DOT_OFFSET_VERT,
			       MEM_DISPLAY_DOT_WIDTH,
			       MEM_DISPLAY_MARGIN_VERT,
			       MEM_DISPLAY_MARGIN_HORIZ,
			       MEM_DISPLAY_OVERFLOW_INDENT,
			       0,
			       0,
			       TRUE, /* frame */
			       ldigit_xpm);

}


