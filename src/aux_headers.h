/*
 * Calcoo: aux_headers.h
 *
 * Copyright (C) 2001 Alexei Kaminski
 *
 */

#ifndef AUX_HEADERS_H
#define AUX_HEADERS_H

/* aux.c */
int  digit_to_code(int);
int  code_to_digit(int);
int  binop_to_od(int);

int  inverse_sign(int);
int  priority(int);

int  last_digit(double);

int  round_double_to_int(double);
int  fact_too_large(double);
double fact_function(double);
double fact_function_jr(double);
int almost_integer(double, double);

double sign_of_double(double) ;
double max_fabs(double, double); 
double smart_sum(double, double, double);


void error_occured(char *, int);
void mess(char *, int);
void messd(char *, double);
void verify_malloc(void *);

#endif /* AUX_HEADERS_H */
