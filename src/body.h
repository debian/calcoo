/*
 * Calcoo: body.h
 *
 * Copyright (C) 2001, 2002, 2005 Alexei Kaminski
 *
 */

#ifndef BODY_H
#define BODY_H

#include "basic.h"
#include "displays.h"

#define SEPARATOR_NUMBER 10

/* 
 * display to display numbers 
 */
typedef struct tbd {
	GtkWidget *minus_sign[INPUT_LENGTH];
	GtkWidget *int_digits[INPUT_LENGTH][BASE];  
	GtkWidget *frac_digits[INPUT_LENGTH][BASE]; 
	GtkWidget *int_ticks[INPUT_LENGTH];
	GtkWidget *frac_ticks[INPUT_LENGTH];
	GtkWidget *dot[INPUT_LENGTH];   
	GtkWidget *E;
	GtkWidget *exp_plus_sign, *exp_minus_sign;
	GtkWidget *exp_digits[EXP_INPUT_LENGTH][BASE]; 
	GtkWidget *error_message;
	GtkWidget *frame;
	int x1;
	int y1;
	int display_exp_indent;
	char ***digit_xpm;
	int cell_width;
	int dot_offset_vert;
	int dot_width;
	int margin_vert;
	int margin_horiz;
	int error_indent;
	int tick_offset_vert;
	int tick_offset_horiz;
} t_body_display ;


/* 
 * display to display few pixmaps (indicators, like deg/rad) 
 */
typedef struct tbsd {
	GtkWidget *glyph[SD_G_MAX];
	GtkWidget *frame;
	int number_of_glyphs;
} t_body_simple_display;


/*
 * the whole calculator body
 */
typedef struct tcb {
/*-----------------------------*/
	GtkWidget *main_window;
	
	/* the parent widget of buttons and displays, 
	 * to prevent their resizing */
	GtkWidget *fixer; 

	GtkStyle  *style;

/*----- buttons and related -------------*/
	GtkWidget *button[MAX_BUTTON_NUMBER];
	GtkWidget *deg_rad_button, *forced_e_button;

	GtkWidget *icon[MAX_BUTTON_NUMBER]; /* button icons */

	GtkTooltips *button_tooltips;
	GtkAccelGroup *accel_group;

	GtkWidget *selection_widget;

/*----- displays -----------------------*/
	/* the main display, memory displays, register displays */
	t_body_display *display[NUMBER_OF_DISPLAYS]; 

	/* deg/rad display, fix/exp/sci display, operation displays */
	t_body_simple_display *simple_display[NUMBER_OF_SIMPLE_DISPLAYS];

	/* not quite a display, just a string to hold the main display contents
	 * used only for "copy" */
	char char_display[INPUT_LENGTH + EXP_INPUT_LENGTH + 5];


/*------ options stored in the body ------*/
	/* (the other options are stored in the cpu, see cpu.h) 
	 * these variables are initialized by the call to load_options()
	 * from main.c */
	int arc_autorelease, hyp_autorelease;

} t_calcoo_body ;

extern t_calcoo_body *body;

#endif /* BODY_H */

