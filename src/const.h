/*
 * Calcoo: const.h
 *
 * Copyright (C) 2001 Alexei Kaminski
 *
 */

#ifndef CONST_H
#define CONST_H

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define PI 3.141592653589793238462643383

#endif /* CONST_H */
