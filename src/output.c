/*
 * Calcoo: output.c
 *
 * Copyright (C) 2001,2002 Alexei Kaminski
 *
 */
#include <string.h>

#include "codes.h"
#include "const.h"
#include "cpu.h"
#include "b_headers.h"
#include "io_headers.h"
#include "aux_headers.h"

void refresh_display(int, t_cpu_display *);
void refresh_char_display(t_cpu_display *);
void refresh_operation_display(int, t_cpu_operation_display *);
void refresh_deg_rad_indicator(int);
void refresh_format_indicator(int);

void refresh_body (void)
{
	int i;

	refresh_format_indicator(cpu->prescribed_format);
	refresh_deg_rad_indicator(cpu->angle_units);

	refresh_display(MAIN_DISPLAY, cpu->d);
	
	for(i = 0; i < NUMBER_OF_MEMS; i++) {
		hide_button_label(CODE_SWITCH_TO_MEM0 + i);
		refresh_display(MEM_DISPLAY_OFFSET + i, cpu->mem_d[i]);
	}

	for(i = 0; i < NUMBER_OF_REG_DISPLAYS; i++) {
		refresh_display(REG_DISPLAY_OFFSET + i, cpu->reg_d[i]);
		refresh_operation_display(i, cpu->op_d[i]);
	}

	show_button_label(CODE_SWITCH_TO_MEM0 + cpu->curr_mem);

	refresh_char_display(cpu->d);

	if (cpu->error == TRUE ) 
		disable_all_buttons(); /* except for C_all and undo */
	else
		enable_all_buttons();
}

void refresh_operation_display(int body_op_d, 
			       t_cpu_operation_display *cpu_op_d)
{
  	clear_simple_display(body_op_d); 
	if (!cpu->rpn_mode) {
		if (cpu_op_d->op_code){
			show_display_glyph(OPERATION_DISPLAY_OFFSET 
					   + body_op_d, 
					   binop_to_od(cpu_op_d->op_code)); 
			/* binop_to_od translates CODE_foo to OD_foo, 
			 * where foo is ADD|SUB|MUL|DIV|POW */
			if (cpu_op_d->show_brace) 
				show_display_glyph(body_op_d, OD_PAREN);  
		}
	}
}

void refresh_deg_rad_indicator(int angle_units)
{
	clear_simple_display(ANGLE_UNIT_DISPLAY_OFFSET);

	if (angle_units == CODE_RAD) {
		show_display_glyph(ANGLE_UNIT_DISPLAY_OFFSET, DRD_RAD);
	} else {
		show_display_glyph(ANGLE_UNIT_DISPLAY_OFFSET, DRD_DEG);
	}
}

void refresh_format_indicator(int format)
{
	clear_simple_display(FORMAT_DISPLAY_OFFSET);

	switch (format){
	case FORMAT_FIX:		
		show_display_glyph(FORMAT_DISPLAY_OFFSET, FD_FIX);
		break;
	case FORMAT_SCI:		
		show_display_glyph(FORMAT_DISPLAY_OFFSET, FD_SCI);
		break;
	case FORMAT_ENG:
		show_display_glyph(FORMAT_DISPLAY_OFFSET, FD_ENG);
		break;
	}
}


void refresh_display(int body_d, t_cpu_display *cpu_d)
{
	int i, leading;

	clear_display(body_d);

	if (cpu_d->display_overflow) {
		show_error_message(body_d);
		return;
	}

	if (cpu_d->n_int == 0) {
		show_int_digit(body_d, INPUT_LENGTH - 1, CODE_0);
		show_dot(body_d, INPUT_LENGTH - 1);
		if (cpu_d->sign == SIGN_MINUS)
			show_minus(body_d, INPUT_LENGTH - 1);
		set_char_display("0");
		return;
	}

	leading = INPUT_LENGTH - (cpu_d->n_int + cpu_d->n_frac);

	if (cpu_d->sign == SIGN_MINUS) 
		show_minus(body_d, leading);


	for (i = 0; i < cpu_d->n_int; i++) 
		show_int_digit(body_d, leading + i, cpu_d->int_field[i]);

	for (i = cpu_d->n_int - 3; i > 0; i-=3) 
		show_int_tick(body_d, leading + i);

	show_dot(body_d, leading + cpu_d->n_int - 1);

	if (cpu_d->n_frac > 0) {
		for (i = 0; i < cpu_d->n_frac; i++)
			show_frac_digit(body_d, leading + cpu_d->n_int + i,
					   cpu_d->frac_field[i]);
		for (i = 3; i < cpu_d->n_frac; i+=3) 
			show_frac_tick(body_d, leading + cpu_d->n_int + i);
	}
	
	if (cpu_d->format == FORMAT_SCI) {
		show_E_and_exp_sign(body_d, cpu_d->exp_sign);
		for (i = 0; i < EXP_INPUT_LENGTH; i++) 
			show_exp_digit(body_d, i, cpu_d->exp_field[i]);
	}
}

void refresh_char_display(t_cpu_display *cpu_d)
{
	int i;
	char s[INPUT_LENGTH + EXP_INPUT_LENGTH + 5];
	char d[10] = "0123456789";

	strcpy(s,"");

	if (cpu_d->sign == SIGN_MINUS)
		strcat(s,"-");

	for (i = 0; i < cpu_d->n_int; i++) 
		strncat(s, &d[code_to_digit(cpu_d->int_field[i])], 1);
		

	if (cpu_d->n_frac > 0) {
		strcat(s,".");
		for (i = 0; i < cpu_d->n_frac; i++)
			strncat(s, &d[code_to_digit(cpu_d->frac_field[i])], 1);
	}
	
	if (cpu_d->format == FORMAT_SCI) {
		strcat(s,"e");
		if (cpu_d->exp_sign == SIGN_MINUS )
			strcat(s,"-");
		for (i = 0; i < EXP_INPUT_LENGTH; i++) 
			strncat(s, &d[code_to_digit(cpu_d->exp_field[i])], 1);
	}

	set_char_display(s);
}

void enable_undo_button(void)
{
	enable_button(CODE_UNDO);
}

void disable_undo_button(void)
{
	disable_button(CODE_UNDO);
}

void enable_redo_button(void)
{
	enable_button(CODE_REDO);
}

void disable_redo_button(void)
{
	disable_button(CODE_REDO);
}
