/*
 * Calcoo: codes.h
 *
 * Copyright (C) 2001, 2002 Alexei Kaminski
 *
 */

#ifndef CODES_H
#define CODES_H

#define SIGN_PLUS   1
#define SIGN_MINUS -1

/* display formats */
#define FORMAT_FIX 0
#define FORMAT_SCI 1
#define FORMAT_ENG 2

#define ENTER_MODE_TRADITIONAL 0
#define ENTER_MODE_HP28        1

#define STACK_MODE_XYZT     0
#define STACK_MODE_INFINITE 1

/*-----------------------------------------------------------------------*/
/* these are the codes to be assigned to cpu->last_action and only for this */

#define ACTION_INPUT 10

#define ACTION_ENTER 11 /* both RPN and algebraic modes
			 * not only the <Enter> key, but many other actions 
			 * actually, for any action that stops input, 
			 * and that is not "Clear" or (in algebraic mode) 
			 * binop 
			 * Actually, the <Enter> key corresponds to this
			 * action only when enter_mode is set to hp28,
			 * otherwise, it corresponds to ACTION_ENTER_PUSH */

#define ACTION_BINOP 12 /* only for algebraic mode, in RPN, use ACTION_ENTER */

#define ACTION_CLEAR 13 /* both RPN and algebraic modes
			 * we need it to be able to get rid of 0 that appears 
			 * on the display when there is nothing to display.
			 * I mean that in the process of input the number
			 * being read is stored in cpu->d (display) rather
			 * than in cpu->x. Since there is zero on the
			 * display before input, setting cpu->last_action
			 * to ACTION_CLEAR signals that this zero must
			 * be removed from the display before we put the 
			 * digits from the input there */

#define ACTION_ENTER_PUSH 14 /* only RPN mode
			      * <Enter> in ENTER_MODE_TRADITIONAL;
			      * in ENTER_MODE_HP28, ACTION_ENTER is used */

#define ACTION_ERROR 15
/*---------------------------------------------------------------------*/

/* the value has the real information about priority:
 * higher values for higher priorities */

#define PRIORITY_CODE_MIN   0 /* for ")" and "=" */
#define PRIORITY_CODE_ADD   1
#define PRIORITY_CODE_MUL   2
#define PRIORITY_CODE_POW   3


/*=====================================================================
 *
 * Codes for buttons
 * 
 * also used as codes for corresponding operations, if applicable
 *
 * the maximum possible code is NUMBER_OF_BUTTONS - 1
 * it is defined in body.h 
 *
 *======================================================================*/

/* The real numerical values of the folowing ten codes are not supposed to be
 * important. The translation must be done by digit_to_code and 
 * code_to_digit. */
#define CODE_0 0
#define CODE_1 1
#define CODE_2 2
#define CODE_3 3
#define CODE_4 4
#define CODE_5 5
#define CODE_6 6
#define CODE_7 7
#define CODE_8 8
#define CODE_9 9

/* attention! careful with these codes!
 * there used to be constructions like 
 * "for (i = CODE_ADD; i <= CODE_POWER; i++)"
 * they must have been eradicated, but who knows...*/
#define CODE_EQ   10
#define CODE_ADD  11
#define CODE_SUB  12
#define CODE_MUL  13
#define CODE_DIV  14
#define CODE_POW  15
#define CODE_ENTER  16

#define CODE_NO_OPERATION 19

#define CODE_DOT      21
#define CODE_SIGN     22
#define CODE_EXP      23
#define CODE_EXP_SIGN 24

#define CODE_LOG   31
#define CODE_10TOX 32
#define CODE_LN    33
#define CODE_ETOX  34
#define CODE_SQRT  35
#define CODE_SQR   36
#define CODE_INVX  37
#define CODE_PI    38
#define CODE_FACT  39

#define CODE_SIN 41
#define CODE_COS 42
#define CODE_TAN 43

#define CODE_DEG 46
#define CODE_RAD 47

#define CODE_ARC 48
#define CODE_HYP 49

#define CODE_EXCH_XY     50
#define CODE_STACK_UP    51
#define CODE_STACK_DOWN  52
#define CODE_LEFT_PAREN  53
#define CODE_RIGHT_PAREN 54

#define CODE_ASIN  61
#define CODE_SINH  62
#define CODE_ASINH 63

#define CODE_ACOS  64
#define CODE_COSH  65
#define CODE_ACOSH 66

#define CODE_ATAN  67
#define CODE_TANH  68
#define CODE_ATANH 69

#define CODE_CLEAR_ALL 100
#define CODE_CLEAR_X   101

#define CODE_DEG_RAD   108
#define CODE_FORMAT    109

#define CODE_MEM_TO_X  110
#define CODE_MEM_PLUS  111
#define CODE_EXCH_XMEM 118
#define CODE_X_TO_MEM  119
#define CODE_SWITCH_TO_MEM0 120
#define CODE_SWITCH_TO_MEM1 121

#define CODE_UNDO      133
#define CODE_REDO      134
#define CODE_INFO      135
#define CODE_SETTINGS  136
#define CODE_COPY      137
#define CODE_PASTE     138

#define CODE_EXIT      150

/* the maximum possible code is NUMBER_OF_BUTTONS - 1,
 * which is defined in body.h */

#endif /* CODES_H */

