/*
 * Calcoo: b_accel.c
 *
 * Copyright (C) 2001 - 2005 Alexei Kaminski
 *
 * all about the keyboard accelerators
 */

#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>

#include "codes.h"
#include "body.h"
#include "defaults.h"
#include "b_headers.h"
#include "gtkaux_headers.h"
#include "io_headers.h"
#include "aux_headers.h"
#include "b_accel_headers.h"


void key_press_handler(GtkWidget* widget, 
		       GdkEventKey* event, 
		       gpointer data)
/* for the keys that cannot be used in GTK accelerator group */
{
  switch (event->keyval)
    {
    case GDK_BackSpace:
	    clicked_code_undo();
	    break;
    case GDK_Delete: 
 	    clicked_code_clear_x(); 
 	    break; 
    case GDK_Up: 
	    clicked_code_stack_up();
 	    break; 
    case GDK_Down: 
	    clicked_code_stack_down();
 	    break; 
    case GDK_Left:
	    clicked_code_undo();
	    break;
    case GDK_Right:
	    clicked_code_redo();
	    break;

    case GDK_0:
    case GDK_KP_0:
	    clicked_code_0();
	    break;
    case GDK_1:
    case GDK_KP_1:
	    clicked_code_1();
	    break;
    case GDK_2:
    case GDK_KP_2:
	    clicked_code_2();
	    break;
    case GDK_3:
    case GDK_KP_3:
	    clicked_code_3();
	    break;
    case GDK_4:
    case GDK_KP_4:
	    clicked_code_4();
	    break;
    case GDK_5:
    case GDK_KP_5:
	    clicked_code_5();
	    break;
    case GDK_6:
    case GDK_KP_6:
	    clicked_code_6();
	    break;
    case GDK_7:
    case GDK_KP_7:
	    clicked_code_7();
	    break;
    case GDK_8:
    case GDK_KP_8:
	    clicked_code_8();
	    break;
    case GDK_9:
    case GDK_KP_9:
	    clicked_code_9();
	    break;

    case GDK_KP_Decimal:
    case GDK_period:
    case GDK_comma:
    case GDK_KP_Separator: /* German keyboards */
	    clicked_code_dot();
	    break;

    case GDK_KP_Add:
    case GDK_plus: 
	    clicked_code_add();
	    break;
    case GDK_KP_Subtract:
    case GDK_minus:
	    if ( GDK_CONTROL_MASK & event->state )
		    clicked_code_sign();
	    else
		    clicked_code_sub();
	    break;
    case GDK_KP_Multiply:
    case GDK_asterisk:
	    clicked_code_mul();
	    break;
    case GDK_KP_Divide:
    case GDK_slash:
	    clicked_code_div();
	    break;
    case GDK_asciicircum:
	    clicked_code_pow();
	    break;


    case GDK_s:
	    clicked_code_sin();
	    break;
    case GDK_c:
	    if ( GDK_CONTROL_MASK & event->state )
		    g_signal_emit_by_name(
			    G_OBJECT(body->button[CODE_COPY]),
			    "clicked");
	    else
		    clicked_code_cos();
	    break;
    case GDK_t:
	    clicked_code_tan();
	    break;

    case GDK_g:
	    clicked_code_log();
	    break;
    case GDK_n:
	    clicked_code_ln();
	    break;

    case GDK_x:
	    clicked_code_etox();
	    break;
    case GDK_d:
	    clicked_code_10tox();
	    break;

    case GDK_q:
	    if ( GDK_CONTROL_MASK & event->state )
		    call_exit();
	    else
		    clicked_code_sqr();
	    break;
    case GDK_w:
	    clicked_code_sqrt();
	    break;

    case GDK_i:
	    clicked_code_invx();
	    break;
    case GDK_p:
	    clicked_code_pi();
	    break;
    case GDK_f:
	    clicked_code_fact();
	    break;

    case GDK_z:
	    if ( GDK_CONTROL_MASK & event->state )
		    clicked_code_undo();
	    break;
    case GDK_r:
	    if ( GDK_CONTROL_MASK & event->state )
		    clicked_code_redo();
	    break;

    case GDK_equal:
	    clicked_code_eq();
	    break;

    case GDK_Return:
    case GDK_KP_Enter:
	    if ( TRUE == get_rpn_mode() )
		    clicked_code_enter();
	    else
		    clicked_code_eq();
	    break;

    case GDK_Escape:
	    clicked_code_clear_all();
	    break;

    case GDK_e:
	    if ( GDK_CONTROL_MASK & event->state )
		    clicked_code_exp_sign();
	    else    
		    clicked_code_exp();
	    break;

    case GDK_parenleft:
    case GDK_bracketleft:
	    clicked_code_left_paren();
	    break;
    case GDK_parenright:
    case GDK_bracketright:
	    clicked_code_right_paren();
	    break;

/* these are special guys; since some action is needed from the body,
 * we cannot send the signal directly to the engine, so we emulate
 * button pressings */

    case GDK_a:
	    g_signal_emit_by_name(G_OBJECT(body->button[CODE_ARC]),
				    "clicked");
	    break;
    case GDK_h:
	    g_signal_emit_by_name(G_OBJECT(body->button[CODE_HYP]),
				    "clicked");
	    break;

    case GDK_v:
	    if ( GDK_CONTROL_MASK & event->state )
		    g_signal_emit_by_name(
			    G_OBJECT(body->button[CODE_PASTE]),
			    "clicked");
	    break;

/* another bunch of special guys; no action on the part of the engine needed */
    case GDK_question:
	    call_info();
	    break;
    case GDK_exclam:
	    call_settings();
	    break;

    default:
	    break;
    }
}

gboolean button_press_handler(GtkWidget* widget, GdkEventButton* event)
{
	if (event->button  == 2 /* middle button*/) 
		g_signal_emit_by_name(G_OBJECT(body->button[CODE_PASTE]),
					"clicked");
	return TRUE;

}

