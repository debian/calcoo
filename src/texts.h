/*
 * Calcoo: texts.h
 *
 * Copyright (C) 2001, 2002, 2003, 2005, 2007 Alexei Kaminski
 *
 */

#ifndef TEXTS_H
#define TEXTS_H

static char *about_text[] = 
{
	" ",
	NULL,
	"Calcoo " VERSION "  -  Scientific calculator",
	NULL,
	"Copyright (C) 2001 - 2007 Alexei Kaminski",
	NULL,
	"Project homepage: http://calcoo.sourceforge.net/",
	NULL,
	"Please report bugs to alexei.kaminski@gmail.com",
	NULL,
	" ",
	NULL, NULL
};

static const gchar *license_text =
{
	"This program is free software; you can redistribute it and/or "
	"modify it under the terms of the GNU General Public License "
	"as published by the Free Software Foundation; either version 2 "
	"of the License, or (at your option) any later version. "
	"\n\n"
	"This program is distributed in the hope that it will be useful, "
	"but WITHOUT ANY WARRANTY; without even the implied warranty of "
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. "
	"See the GNU General Public License for more details. "
	"\n\n"
	"You should have received a copy of the GNU General Public License "
	"along with this program; if not, write to the Free Software "
	"Foundation, Inc., 59 Temple Place - Suite 330, "
	"Boston, MA  02111-1307, USA. "
};

static const gchar *help_text =
{
	"Even though it must be clear in general how to use calcoo, "
	"some subtleties may need clarification. "
	"\n\n"
	"In the RPN mode, the stack either has four registers, X, Y, Z, T, "
	"like in most real RPN calculators, or is unlimited, depending "
	"on the corresponding option. In the algebraic mode, the number of "
	"registers is unlimited, but only the lowest four have their "
	"contents displayed. "
	"\n\n"
	"Several adjacent opening parens are possible. However, calcoo "
	"will show only one paren in the corresponding display, due to "
	"display limitations. Calcoo does keep track of all of them, even "
	"though it is unable to show them. "
	"\n\n"
	"The closing paren button acts as \"=\" button if there are no "
	"matching opening parens. "
	"\n\n"
	"If an expression begins with an opening paren, the paren will "
	"not be displayed (actually the paren will be ignored). "
	"Nevertheless, you should go ahead and type the whole expression, "
	"since an unmatched closing paren acts as \"=\", the whole "
	"expression will be evaluated properly, as though the (ignored) "
	"opening paren and the closing paren (interpreted as \"=\") were "
	"interpreted as parens. "
	"\n\n"
	"Undo remembers only the last 64 elementary actions. "
	"\n\n"
	"Switching between RPN and algebraic modes does reset undo. "
};

static const gchar *shortcuts_text[] =
{
	"Esc", "clear all registers",
	"Delete", "clear register X",
	"0 1 2 3 4 5 6 7 8 9", "corresponding digits",
	". ,",  ".",
	"+ - * / ^", "corresponding binary operations",
	"s c t", "sin cos tan",
	"n g", "ln log",
	"x d", "e^x 10^x",
	"q w", "x^2 sqrt",
	"i p f", "1/x pi x!",
	"a h", "arc hyp",
	"( [  ] )", "( )",
	"Enter, =", "evaluate (algebraic mode)",
	"Enter", "enter (RPN mode)",
	"BackSpace Ctrl-z LeftArrow", "undo",
	"Ctrl-r RightArrow", "redo",
	"Ctrl-c Ctrl-v", "copy paste",
	"Ctrl--", "change sign",
	"UpArrow/DownArrow", "scroll stack up/down (RPN mode)",
	"e", "E",
	"Ctrl-e", "change exp sign",
	"?", "info/help",
	"!", "options",
	"Ctrl-q", "exit",
	NULL, NULL, NULL, NULL
};

static char *version_console[] =
{
	PACKAGE " " VERSION,
	NULL
};

static char *help_console[] =
{
	"",
	"Calcoo - scientific calculator (GTK+) Version " VERSION,
	"Copyright (C) 2001 - 2007 Alexei Kaminski (alexei.kaminski@gmail.com)",
	"Usage: calcoo [OPTION]",
	"",
	"Valid options:",
	"    -v, --version  for version info",
	"    -h, --help     to see this help",
	"",
	NULL
};

static char *invalid_console[] =
{
	"Invalid option. Run \"calcoo -h\" to see the list of valid options.",
	NULL
};

#endif
