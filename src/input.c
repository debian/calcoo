/*
 * Calcoo: input.c
 *
 * Copyright (C) 2001-2005 Alexei Kaminski
 *
 */

#include "codes.h"
#include "cpu.h"
#include "io_headers.h"
#include "c_headers.h"
#include "b_headers.h"
#include "aux_headers.h"

/* most of the call_foo() functions are in the cpu part, though
 * some of them are in the body part */

/* digit keys */

void clicked_code_0(void)
{
	call_digit(CODE_0);
}

void clicked_code_1(void)
{
	call_digit(CODE_1);
}

void clicked_code_2(void)
{
	call_digit(CODE_2);
}

void clicked_code_3(void)
{
	call_digit(CODE_3);
}

void clicked_code_4(void)
{
	call_digit(CODE_4);
}

void clicked_code_5(void)
{
	call_digit(CODE_5);
}

void clicked_code_6(void)
{
	call_digit(CODE_6);
}

void clicked_code_7(void)
{
	call_digit(CODE_7);
}

void clicked_code_8(void)
{
	call_digit(CODE_8);
}

void clicked_code_9(void)
{
	call_digit(CODE_9);
}


/* other input keys */

void clicked_code_dot(void)
{
	call_dot();
}

void clicked_code_sign(void)
{
	call_sign();
}

void clicked_code_exp(void)
{
	call_exp();
}

void clicked_code_exp_sign(void)
{
	call_exp_sign();
}

void clicked_code_clear_x(void)
{
	call_clear_x();
}

/* 3.1415 */

void clicked_code_pi(void)
{
	call_pi();
}

/* binary operation keys */

void clicked_code_add(void)
{
	call_binary_op(CODE_ADD);
}

void clicked_code_sub(void)
{
	call_binary_op(CODE_SUB);
}

void clicked_code_mul(void)
{
	call_binary_op(CODE_MUL);
}

void clicked_code_div(void)
{
	call_binary_op(CODE_DIV);
}

void clicked_code_pow(void)
{
	call_binary_op(CODE_POW);
}

/* memory operation keys */

void clicked_code_mem_plus(void)
{
	call_mem_op(CODE_MEM_PLUS);
}

void clicked_code_mem_to_x(void)
{
	call_mem_op(CODE_MEM_TO_X);
}

void clicked_code_x_to_mem(void)
{
	call_mem_op(CODE_X_TO_MEM);
}

void clicked_code_exch_xmem(void)
{
	call_mem_op(CODE_EXCH_XMEM);
}

void clicked_code_switch_to_mem0(void)
{
	call_switch_to_mem(0);
}

void clicked_code_switch_to_mem1(void)
{
	call_switch_to_mem(1);
}

/* unary operation keys */

void clicked_code_10tox(void)
{
	call_unary_op(CODE_10TOX);
}

void clicked_code_log(void)
{
	call_unary_op(CODE_LOG);
}

void clicked_code_etox(void)
{
	call_unary_op(CODE_ETOX);
}

void clicked_code_ln(void)
{
	call_unary_op(CODE_LN);
}

void clicked_code_sqr(void)
{
	call_unary_op(CODE_SQR);
}

void clicked_code_sqrt(void)
{
	call_unary_op(CODE_SQRT);
}

void clicked_code_invx(void)
{
	call_unary_op(CODE_INVX);
}

void clicked_code_fact(void)
{
	call_unary_op(CODE_FACT);
}

/* equals key */

void clicked_code_eq(void)
{
	call_eq ();
}

/* CA key */

void clicked_code_clear_all(void)
{
	call_clear_all ();
}

void clicked_code_exch_xy(void)
{
	call_exch_xy ();
}

void clicked_code_stack_up(void)
{
	call_stack_up ();
}

void clicked_code_stack_down(void)
{
	call_stack_down ();
}

/* trigonometry keys */

void clicked_code_sin(void)
{
	if (!get_hyp_state()) 
		if (!get_arc_state())
			call_unary_op(CODE_SIN);
		else
			call_unary_op(CODE_ASIN);
	else
		if (!get_arc_state()) 
			call_unary_op(CODE_SINH);
		else
			call_unary_op(CODE_ASINH);

	raise_arc_hyp();
}

void clicked_code_cos(void)
{
	if (!get_hyp_state()) 
		if (!get_arc_state()) 
			call_unary_op(CODE_COS);
		else 
			call_unary_op(CODE_ACOS);
	else 
		if (!get_arc_state()) 
			call_unary_op(CODE_COSH);
		else
			call_unary_op(CODE_ACOSH);

	raise_arc_hyp();
}

void clicked_code_tan(void)
{
	if (!get_hyp_state()) 
		if (!get_arc_state()) 
			call_unary_op(CODE_TAN);
		else 
			call_unary_op(CODE_ATAN);
	else 
		if (!get_arc_state()) 
			call_unary_op(CODE_TANH);
		else 
			call_unary_op(CODE_ATANH);

	raise_arc_hyp();
}

void clicked_change_display_format(void)
{
	call_change_display_format();
}

void clicked_code_undo(void)
{
	call_undo ();
}

void clicked_code_redo(void)
{
	call_redo ();
}

void clicked_code_info(void)
{
	call_info (); /* call_info() is in b_info.c */
}

void clicked_code_settings(void)
{
	call_settings (); /* call_settings() is in b_settings.c */
}

void clicked_code_left_paren(void)
{
	call_left_paren();
}

void clicked_code_right_paren(void)
{
	call_right_paren();
}

void clicked_code_enter(void)
{
	call_enter();
}

void clicked_code_paste(double x)
{
	call_import_paste(x);
}

/* void clicked_code_copy(void) */
/* No such function. The Copy button is connected
 * to special GTK functions, see b_buttons.c */

void clicked_change_angle_units(void)
{
	call_change_angle_units();
	save_settings();
}

void clicked_set_angle_units(int a)
{
	call_set_angle_units(a);
}

int requested_angle_units(void)
{
	int a;
	a = call_get_angle_units();
	return a;
}


void clicked_set_rpn_mode(int a)
{
	call_set_rpn_mode(a);
}

int requested_rpn_mode(void)
{
	int a;
	a = call_get_rpn_mode();
	return a;
}

void clicked_set_enter_mode(int a)
{
	call_set_enter_mode(a);
}

int requested_enter_mode(void)
{
	int a;
	a = call_get_enter_mode();
	return a;
}

void clicked_set_rounding_mode(int a)
{
	call_set_rounding_mode(a);
}

int requested_rounding_mode(void)
{
	int a;
	a = call_get_rounding_mode();
	return a;
}

void clicked_set_trunc_zeros_mode(int a)
{
	call_set_trunc_zeros_mode(a);
}

int requested_trunc_zeros_mode(void)
{
	int a;
	a = call_get_trunc_zeros_mode();
	return a;
}

void clicked_set_stack_mode(int a)
{
	call_set_stack_mode(a);
}

int requested_stack_mode(void)
{
	int a;
	a = call_get_stack_mode();
	return a;
}

/* void clicked_code_exit(void) */
/* { */
/* 	call_exit();  */
/* } */
/* call_exit() is in b_main.c */
