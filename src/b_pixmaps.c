/*
 * Calcoo: b_pixmaps.c
 *
 * Copyright (C) 2001 Alexei Kaminski
 *
 * determines pixmaps to be used
 *
 */

#include <stdlib.h>

#include "codes.h"
#include "displays.h"
#include "b_headers.h"

/* loading pixmaps for button icons */
#include "pixmaps/b0.xpm"
#include "pixmaps/b1.xpm"
#include "pixmaps/b2.xpm"
#include "pixmaps/b3.xpm"
#include "pixmaps/b4.xpm"
#include "pixmaps/b5.xpm"
#include "pixmaps/b6.xpm"
#include "pixmaps/b7.xpm"
#include "pixmaps/b8.xpm"
#include "pixmaps/b9.xpm"
#include "pixmaps/plus.xpm"
#include "pixmaps/minus.xpm"
#include "pixmaps/mult.xpm"
#include "pixmaps/div.xpm"
#include "pixmaps/eq.xpm"
#include "pixmaps/cos.xpm"
#include "pixmaps/sin.xpm"
#include "pixmaps/tan.xpm"
#include "pixmaps/arc.xpm"
#include "pixmaps/hyp.xpm"
#include "pixmaps/sqr.xpm"
#include "pixmaps/etox.xpm"
#include "pixmaps/b10tox.xpm"
#include "pixmaps/power.xpm"
#include "pixmaps/pi.xpm"
#include "pixmaps/log.xpm"
#include "pixmaps/ln.xpm"
#include "pixmaps/fact.xpm"
#include "pixmaps/invx.xpm"
#include "pixmaps/sqrt.xpm"
#include "pixmaps/exp.xpm"
#include "pixmaps/exp_sign.xpm"
#include "pixmaps/sign.xpm"
#include "pixmaps/clear_x.xpm"
#include "pixmaps/dot.xpm"
#include "pixmaps/undo.xpm"
#include "pixmaps/redo.xpm"
#include "pixmaps/clear_all.xpm"
#include "pixmaps/exch_xy.xpm"
#include "pixmaps/down.xpm"
#include "pixmaps/up.xpm"
#include "pixmaps/mem_plus.xpm"
#include "pixmaps/mem_to_x.xpm"
#include "pixmaps/x_to_mem.xpm"
#include "pixmaps/exch_xmem.xpm"
#include "pixmaps/question.xpm"
#include "pixmaps/exclamation.xpm"
#include "pixmaps/left_paren.xpm"
#include "pixmaps/right_paren.xpm"
#include "pixmaps/copy.xpm"
#include "pixmaps/paste.xpm"
#include "pixmaps/push.xpm"

#include "pixmaps/curr_mem.xpm"

/* pixmaps for the main display */
#include "pixmaps/d0.xpm"
#include "pixmaps/d1.xpm"
#include "pixmaps/d2.xpm"
#include "pixmaps/d3.xpm"
#include "pixmaps/d4.xpm"
#include "pixmaps/d5.xpm"
#include "pixmaps/d6.xpm"
#include "pixmaps/d7.xpm"
#include "pixmaps/d8.xpm"
#include "pixmaps/d9.xpm"
#include "pixmaps/ddot.xpm"
#include "pixmaps/de.xpm"
#include "pixmaps/dplus.xpm"
#include "pixmaps/dminus.xpm"
#include "pixmaps/derror.xpm"
#include "pixmaps/tick.xpm"

/* pixmaps for little displays */
#include "pixmaps/l0.xpm"
#include "pixmaps/l1.xpm"
#include "pixmaps/l2.xpm"
#include "pixmaps/l3.xpm"
#include "pixmaps/l4.xpm"
#include "pixmaps/l5.xpm"
#include "pixmaps/l6.xpm"
#include "pixmaps/l7.xpm"
#include "pixmaps/l8.xpm"
#include "pixmaps/l9.xpm"
#include "pixmaps/ldot.xpm"
#include "pixmaps/le.xpm"
#include "pixmaps/lplus.xpm"
#include "pixmaps/lminus.xpm"
#include "pixmaps/lerror.xpm"

/* pixmaps for the operation displays */
#include "pixmaps/oplus.xpm"
#include "pixmaps/ominus.xpm"
#include "pixmaps/omult.xpm"
#include "pixmaps/odiv.xpm"
#include "pixmaps/opower.xpm"
#include "pixmaps/obrace.xpm"

/* pixmaps for the deg/rad switch */
#include "pixmaps/deg.xpm"
#include "pixmaps/rad.xpm"

/* pixmaps for the display format switch */
#include "pixmaps/frm_sci.xpm"
#include "pixmaps/frm_fix.xpm"
#include "pixmaps/frm_eng.xpm"

/* pixmaps for the display labels */
#include "pixmaps/oy.xpm"
#include "pixmaps/oz.xpm"
#include "pixmaps/ot.xpm"

/* pixmap for buttons without labels */
#include "pixmaps/empty.xpm"

void assign_pixmaps(char ***button_xpm,
		    char ***digit_xpm, 
		    char ***ldigit_xpm,
		    char ***op_xpm,
		    char ***deg_rad_xpm,
		    char ***format_xpm,
		    char ***reg_display_label_xpm)
{
/* Main display */
	digit_xpm[0] = d0_xpm;
	digit_xpm[1] = d1_xpm;
	digit_xpm[2] = d2_xpm;
	digit_xpm[3] = d3_xpm;
	digit_xpm[4] = d4_xpm;
	digit_xpm[5] = d5_xpm;
	digit_xpm[6] = d6_xpm;
	digit_xpm[7] = d7_xpm;
	digit_xpm[8] = d8_xpm;
	digit_xpm[9] = d9_xpm;
	digit_xpm[D_E] = de_xpm;
	digit_xpm[D_PLUS] = dplus_xpm;
	digit_xpm[D_MINUS] = dminus_xpm;
	digit_xpm[D_DOT] = ddot_xpm;
	digit_xpm[D_TICK] = tick_xpm;
	digit_xpm[D_OVERFLOW] = derror_xpm;

/* Little displays */
	ldigit_xpm[0] = l0_xpm;
	ldigit_xpm[1] = l1_xpm;
	ldigit_xpm[2] = l2_xpm;
	ldigit_xpm[3] = l3_xpm;
	ldigit_xpm[4] = l4_xpm;
	ldigit_xpm[5] = l5_xpm;
	ldigit_xpm[6] = l6_xpm;
	ldigit_xpm[7] = l7_xpm;
	ldigit_xpm[8] = l8_xpm;
	ldigit_xpm[9] = l9_xpm;
	ldigit_xpm[D_E] = le_xpm;
	ldigit_xpm[D_PLUS] = lplus_xpm;
	ldigit_xpm[D_MINUS] = lminus_xpm;
	ldigit_xpm[D_DOT] = ldot_xpm;
	ldigit_xpm[D_TICK] = empty_xpm;
	ldigit_xpm[D_OVERFLOW] = lerror_xpm;

/* Operation displays */
	op_xpm[OD_ADD] = oplus_xpm;
	op_xpm[OD_SUB] = ominus_xpm;
	op_xpm[OD_MUL] = omult_xpm;
	op_xpm[OD_DIV] = odiv_xpm;
	op_xpm[OD_POW] = opower_xpm;
	op_xpm[OD_PAREN] = obrace_xpm;

/* Labels for the register displays */
	reg_display_label_xpm[0] = oy_xpm;
	reg_display_label_xpm[1] = oz_xpm;
	reg_display_label_xpm[2] = ot_xpm;

/* deg/rad switch */
  	deg_rad_xpm[DRD_DEG] = deg_xpm;
  	deg_rad_xpm[DRD_RAD] = rad_xpm;

/* format switch */
	format_xpm[FD_FIX] = frm_fix_xpm;
	format_xpm[FD_SCI] = frm_sci_xpm;
	format_xpm[FD_ENG] = frm_eng_xpm;
	
/* Button labels */
	button_xpm[CODE_INFO] = question_xpm;
	button_xpm[CODE_SETTINGS] = exclamation_xpm;
	button_xpm[CODE_DEG_RAD] = empty_xpm;
	button_xpm[CODE_FORMAT] = empty_xpm;

	button_xpm[CODE_COPY] = copy_xpm;
	button_xpm[CODE_PASTE] = paste_xpm;

	button_xpm[CODE_SIN] = sin_xpm;
	button_xpm[CODE_COS] = cos_xpm;
	button_xpm[CODE_TAN] = tan_xpm;
	button_xpm[CODE_ARC] = arc_xpm;
	button_xpm[CODE_HYP] = hyp_xpm;

	button_xpm[CODE_SQR] = sqr_xpm;
	button_xpm[CODE_SQRT] = sqrt_xpm;
	button_xpm[CODE_ETOX] = etox_xpm;	
	button_xpm[CODE_LN] = ln_xpm;
	button_xpm[CODE_10TOX] = b10tox_xpm;
	button_xpm[CODE_LOG] = log_xpm;
	button_xpm[CODE_INVX] = invx_xpm;
	button_xpm[CODE_PI] = pi_xpm;
	button_xpm[CODE_FACT] = fact_xpm;

	button_xpm[CODE_0] = b0_xpm;
	button_xpm[CODE_1] = b1_xpm;
	button_xpm[CODE_2] = b2_xpm;
	button_xpm[CODE_3] = b3_xpm;
	button_xpm[CODE_4] = b4_xpm;
	button_xpm[CODE_5] = b5_xpm;
	button_xpm[CODE_6] = b6_xpm;
    	button_xpm[CODE_7] = b7_xpm;    
	button_xpm[CODE_8] = b8_xpm;
	button_xpm[CODE_9] =b9_xpm;

	button_xpm[CODE_SIGN] = sign_xpm;
	button_xpm[CODE_DOT] = dot_xpm;
	button_xpm[CODE_EXP] = exp_xpm;
	button_xpm[CODE_EXP_SIGN] = exp_sign_xpm;

	button_xpm[CODE_ADD] = plus_xpm;
	button_xpm[CODE_SUB] = minus_xpm;
	button_xpm[CODE_MUL] = mult_xpm;
	button_xpm[CODE_DIV] = div_xpm;
	button_xpm[CODE_POW] = power_xpm;

	button_xpm[CODE_UNDO] = undo_xpm;
	button_xpm[CODE_REDO] = redo_xpm;

	button_xpm[CODE_CLEAR_ALL] = clear_all_xpm;
	button_xpm[CODE_EQ] = eq_xpm;
	button_xpm[CODE_ENTER] = push_xpm;

	button_xpm[CODE_EXCH_XY] = exch_xy_xpm;
	button_xpm[CODE_STACK_DOWN] = down_xpm;
	button_xpm[CODE_STACK_UP] = up_xpm;

	button_xpm[CODE_LEFT_PAREN] = left_paren_xpm;
	button_xpm[CODE_RIGHT_PAREN] = right_paren_xpm;

	button_xpm[CODE_CLEAR_X] = clear_x_xpm;

	button_xpm[CODE_X_TO_MEM] = x_to_mem_xpm;
	button_xpm[CODE_MEM_TO_X] = mem_to_x_xpm;
	button_xpm[CODE_MEM_PLUS] = mem_plus_xpm;
	button_xpm[CODE_EXCH_XMEM] = exch_xmem_xpm;
	button_xpm[CODE_SWITCH_TO_MEM0] = curr_mem_xpm;
	button_xpm[CODE_SWITCH_TO_MEM1] = curr_mem_xpm;
}
