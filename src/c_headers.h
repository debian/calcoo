/*
 * Calcoo: c_headers.h
 *
 * Copyright (C) 2001,2002 Alexei Kaminski
 *
 */

#ifndef CPU_HEADERS_H
#define CPU_HEADERS_H

/* c_input.c */
void call_digit(int);  
void call_clear_all(void);
void call_clear_x(void);
void call_dot(void);
void call_pi(void);
void call_exp(void); /* this is switching to enter the exponent, not e^x */
void call_sign(void);
void call_exp_sign(void);
void call_import_paste(double);
void call_change_display_format(void);

/* c_mem.c */
void call_mem_op(int);
void call_switch_to_mem(int m);

/* c_op.c */
void call_binary_op(int);
void call_unary_op(int);
void call_eq(void);
void call_enter(void); 
void call_exch_xy(void);
void call_stack_up(void);
void call_stack_down(void);
void call_left_paren(void);
void call_right_paren(void);
void push_stack(void);
void pop_stack(void);

/* c_main.c */
void reset_registers(void);
void reset_input(void);
void input_to_x(void);
void init_cpu(void);
void call_set_rpn_mode(int);
int  call_get_rpn_mode(void);
void call_set_enter_mode(int);
int  call_get_enter_mode(void);
void call_set_rounding_mode(int);
int  call_get_rounding_mode(void);
void call_set_trunc_zeros_mode(int);
int  call_get_trunc_zeros_mode(void);
void call_set_stack_mode(int);
int call_get_stack_mode(void);
void call_change_angle_units(void);
int call_get_angle_units(void);
void call_set_angle_units(int);
void aftermath(void);
/* c_output.c */
void cpu_to_output(void);

/* c_undo.c */
void init_undo_stack(void);
void reset_undo_stack(void);
void call_undo(void);
void call_redo(void);
void save_for_undo(void);

#endif /* CPU_HEADERS_H */
