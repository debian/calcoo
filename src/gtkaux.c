/*
 * Calcoo: gtkaux.c
 *
 * Copyright (C) 2001, 2002, 2005, 2007 Alexei Kaminski
 *
 * auxiliary functions for operations with gtk widgets
 * no functions related to specific calcoo tasks
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "codes.h"
#include "body.h"
#include "defaults.h"
#include "gtkaux_headers.h"
#include "aux_headers.h"
#include "io_headers.h" 

void create_fg_pixmap(GtkWidget **pixmap_widget, char **pixmap_xpm)
/* creates a monochrome gtk pixmap having the color of the gtk foreground 
 * out of the given pixmap */
{
	GdkPixmap *pixmap, *all_black;
	GdkBitmap *mask;
	int w, h, tmp1, tmp2;
	int i;
	char *black_bits;
	int bitmap_size;

	/* getting the transparency mask from the original pixmap */
	pixmap = gdk_pixmap_create_from_xpm_d ( 
		        (body->main_window)->window,
			&(mask),
			&(body->style)->bg[GTK_STATE_NORMAL],
			(gchar **) pixmap_xpm 
		 );

	/* getting the size of the original pixmap */
	sscanf(pixmap_xpm[0],"%d %d %d %d", &w, &h, &tmp1, &tmp2);

	/* 7 and 8 here are not magic numbers! 
	 * 8 is the number of bits in a byte
	 * 7 is 8 - 1 */
	bitmap_size = ((w + 7) / 8) * h;

	/* creating a bitmap with all bits on, 
	 * of the size of the original pixmap */
	black_bits = calloc(sizeof(char), bitmap_size);

        /* turning on all the pixels in the bitmap */
	for (i = 0; i < bitmap_size; i++)
		black_bits[i] = 0xff;

	/* creating a bitmap with all pixels having the color of foreground, 
	 * of the size of the original pixmap */
	all_black = gdk_pixmap_create_from_data(
		(body->main_window)->window,
		black_bits,
		w, h,
		-1,
		&(body->style)->fg[GTK_STATE_FOR_CALCOO_FOREGROUND_COLOR],
		&(body->style)->bg[GTK_STATE_NORMAL]);
	
	if (CONVERT_ICONS_TO_FOREGROUND_COLOR)
		/* imposing the transparency mask of the original pixmap
		 * onto the foreground rectangle */
		*pixmap_widget = gtk_image_new_from_pixmap(all_black, mask);
	else
		*pixmap_widget = gtk_image_new_from_pixmap(pixmap, mask);

	free(black_bits);
	g_object_unref(pixmap);
	g_object_unref(all_black);
	g_object_unref(mask);
}

void create_n_put_pixmap(GtkWidget **pixmap_widget, int x, int y, 
			 char **pixmap_xpm)
{
	create_fg_pixmap(pixmap_widget, pixmap_xpm);
	gtk_fixed_put(GTK_FIXED(body->fixer), *pixmap_widget, x, y);
}

void own_selection( GtkWidget *widget,
		    gint      *have_selection )
{
	*have_selection = gtk_selection_owner_set (body->selection_widget,
						   GDK_SELECTION_PRIMARY,
						   GDK_CURRENT_TIME);
}

void set_selection( GtkWidget        *widget, 
		    GtkSelectionData *selection_data,
		    guint             info,
		    guint             time_stamp,
		    gpointer          data )
{
	gtk_selection_data_set (selection_data, 
				GDK_SELECTION_TYPE_STRING, 
				8, 
 				(unsigned char*)body->char_display,  
 				strlen(body->char_display)
		); 
}

gboolean selection_clear( GtkWidget         *widget,
			  GdkEventSelection *event,
			  gint              *have_selection )
{
	*have_selection = FALSE;
	return TRUE;
}

void request_selection( GtkWidget *widget,
			gpointer   data )
{
  static GdkAtom targets_atom = GDK_NONE;
  GtkWidget *window = (GtkWidget *)data;	

  /* Get the atom corresponding to the string "TARGETS" */
  if (targets_atom == GDK_NONE)
    targets_atom = gdk_atom_intern ("STRING", FALSE);

  /* And request the "TARGETS" target for the primary selection */
  gtk_selection_convert (window, GDK_SELECTION_PRIMARY, targets_atom,
			 GDK_CURRENT_TIME);
}

#define MAX_SELECTION_LENGTH 20

void get_selection(GtkWidget *widget, GtkSelectionData *data, gpointer val)
{
	char buff[MAX_SELECTION_LENGTH];
	int success;
	double x;

	if( data->length + 1 > MAX_SELECTION_LENGTH )
		return; /* selection too long */

	if(data->length < 0) /* the selection could not be retrieved */
		return;
	strncpy( buff, (char*)data->data, data->length );
	buff[data->length] = '\n';
	success = sscanf(buff,"%lf", &x);
	if (success)
		clicked_code_paste(x);
}

