Source: calcoo
Section: utils
Priority: optional
Maintainer: Jonathan Carter <jcc@debian.org>
Build-Depends: debhelper-compat (= 12), libgtk2.0-dev, libglib2.0-dev
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/calcoo
Vcs-Git: https://salsa.debian.org/debian/calcoo.git
Homepage: http://calcoo.sourceforge.net/

Package: calcoo
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Scientific calculator (GTK+)
 Calcoo is a scientific calculator designed to provide maximum usability.
 The features that make Calcoo better than (at least some) other calculator
 programs are:
 .
  - bitmapped button labels and display digits to improve readability
  - no double-function buttons - you need to click only one button
    for any operation (except for arc-hyp trigonometric functions)
  - undo/redo buttons
  - both RPN (reverse Polish notation) and algebraic modes
  - copy/paste interaction with X clipboard
  - display tick marks to separate thousands
  - two memory registers with displays
  - displays for Y, Z, and T registers
