/*
 * Calcoo: displays.h
 *
 * Copyright (C) 2001, 2002 Alexei Kaminski
 *
 */

#ifndef DISPLAYS_H
#define DISPLAYS_H

#include "basic.h" /* for the NUMBER_OF_MEMS */

#define NUMBER_OF_REG_DISPLAYS 3 /* if you want to change this number, you 
				  * also need to make modifications in 
				  * cpu_to_output() */


#define NUMBER_OF_DISPLAYS ( 1 + NUMBER_OF_MEMS + NUMBER_OF_REG_DISPLAYS )

#define NUMBER_OF_SIMPLE_DISPLAYS (NUMBER_OF_REG_DISPLAYS * 2 + 2)
/* NUMBER_OF_REG_DISPLAYS for display labels,
 * NUMBER_OF_REG_DISPLAYS for the operations and parens in the stack,
 * 1 for display format and 1 for deg/rad 
 * in the land of Mordor, where the shadow lies */


/* --------------------------------------------------------
 * positions of the displays in the array which holds them 
 * -------------------------------------------------------- */

#define MAIN_DISPLAY 0
#define MEM_DISPLAY_OFFSET 1 /* the number of the first mem display */
#define REG_DISPLAY_OFFSET (1 + NUMBER_OF_MEMS) /* of the first reg display */


#define OPERATION_DISPLAY_OFFSET 0
#define DISPLAY_LABEL_DISPLAY_OFFSET NUMBER_OF_REG_DISPLAYS
#define ANGLE_UNIT_DISPLAY_OFFSET (NUMBER_OF_REG_DISPLAYS * 2)
#define FORMAT_DISPLAY_OFFSET (NUMBER_OF_REG_DISPLAYS * 2 + 1)


/* ----------------------------------------------
 * codes for the display glyphs other than digits
 * 
 * when a display is created, the set of the glyphs to be used
 * is passed to the corresponding function in an array; the codes
 * label the positions of the glyphs in this array 
 * ----------------------------------------------- */

/* 0-9 reserved for the digits */
#define D_E     10
#define D_PLUS  11
#define D_MINUS 12
#define D_DOT   13
#define D_TICK  14
#define D_OVERFLOW  15
/* don't forget to adjust D_G_TOTAL if you add something to
 * the set below */

#define NUMBER_OF_DISPLAY_GLYPHS 16 
		     /* 0-9 plus the glyphs defined above 
		      * it is needed in the decaration of the array,
		      * which holds the glyphs to pass to the corresp.
		      * function for the display creation */


/* ---------------------------------------------------
 * codes for the glyphs of the "operation displays,"
 * which are small displays showing operations and parens in the stack
 * --------------------------------------------------- */

#define OD_ADD  0
#define OD_SUB  1
#define OD_MUL  2
#define OD_DIV  3
#define OD_POW  4
#define OD_PAREN 5

#define NUMBER_OF_OPERATION_DISPLAY_GLYPHS  6 


/* ---------------------------------------------------
 * codes for the glyphs of the deg/rad display
 * --------------------------------------------------- */

#define DRD_DEG 0
#define DRD_RAD 1

#define NUMBER_OF_ANGLE_UNIT_GLYPHS 2


/* ---------------------------------------------------
 * codes for the glyphs of the fix/exp/sci display
 * --------------------------------------------------- */

#define FD_FIX 0
#define FD_SCI 1
#define FD_ENG 2

#define NUMBER_OF_FORMAT_GLYPHS 3

#endif  /* DISPLAYS_H */
