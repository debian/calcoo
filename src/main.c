/*
 * Calcoo: main.c
 *
 * Copyright (C) 2001, 2002 Alexei Kaminski
 *
 * See README for the information on the licence, source structure etc.
 *
 */

#include <gtk/gtk.h>

#include "c_headers.h"
#include "b_headers.h"
#include "aux_headers.h"

int main (int argc, char *argv[])
{
	mess("Calcoo started in the DEBUG mode ", 0); 
	/* This produces output only if  the variable CALCOO_DEBUG has been
	 * defined by the configure script,and is introduced as an easy
	 * indication whether we are in the debugging mode or not */

	if (argc > 1 ) {
		process_options(argv[1]);
		return 0; /* Do not start calcoo if any command-line options
			   * were given. Quite reasonable, since the only
			   * valid options are --help and --version */
	}
	
	gtk_init(&argc, &argv); 

     	create_body();    /* create all the widgets */

      	init_cpu();       /* create and initialize the engine */

	load_settings();   /* ...from ~/.calcoo */

	gtk_main();       /* start calcoo */

	return 0;
}

