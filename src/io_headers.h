/*
 * Calcoo: io_headers.h
 *
 * Copyright (C) 2001-2005 Alexei Kaminski
 *
 */


#ifndef IO_HEADERS_H
#define IO_HEADERS_H

/* input.c */
void clicked_code_0(void);
void clicked_code_1(void);
void clicked_code_2(void);
void clicked_code_3(void);
void clicked_code_4(void);
void clicked_code_5(void);
void clicked_code_6(void);
void clicked_code_7(void);
void clicked_code_8(void);
void clicked_code_9(void);
void clicked_code_add(void);
void clicked_code_sub(void);
void clicked_code_mul(void);
void clicked_code_div(void);
void clicked_code_pow(void);
void clicked_code_ln(void);
void clicked_code_log(void);
void clicked_code_10tox(void);
void clicked_code_etox(void);
void clicked_code_sqr(void);
void clicked_code_sqrt(void);
void clicked_code_invx(void);
void clicked_code_fact(void);
void clicked_code_pi(void);
void clicked_code_eq(void);
void clicked_code_clear_all(void);
void clicked_code_dot(void);
void clicked_code_exp(void);
void clicked_code_sign(void);
void clicked_code_exp_sign(void);
void clicked_code_clear_x(void);
void clicked_code_mem_to_x(void);
void clicked_code_mem_plus(void);
void clicked_code_x_to_mem(void);
void clicked_code_exch_xmem(void);
void clicked_code_switch_to_mem0(void);
void clicked_code_switch_to_mem1(void);
void clicked_code_exch_xy(void);
void clicked_code_stack_up(void);
void clicked_code_stack_down(void);
void clicked_code_sin(void);
void clicked_code_cos(void);
void clicked_code_tan(void);
void clicked_change_display_format(void);
void clicked_code_undo(void);
void clicked_code_redo(void);
void clicked_code_info(void);
void clicked_code_settings(void);
void clicked_code_left_paren(void);
void clicked_code_right_paren(void);
void clicked_code_paste(double);
void clicked_code_enter(void);
void clicked_set_rpn_mode(int);
void clicked_change_angle_units(void);
void clicked_set_angle_units(int);
int requested_angle_units(void);
int requested_rpn_mode(void);
void clicked_set_enter_mode(int);
int requested_enter_mode(void);
void clicked_set_rounding_mode(int);
int requested_rounding_mode(void);
void clicked_set_trunc_zeros_mode(int);
int requested_trunc_zeros_mode(void);
void clicked_set_stack_mode(int);
int requested_stack_mode(void);
/* void clicked_code_exit(void); */

/* output.c */
void refresh_body(void);
void enable_undo_button(void);
void disable_undo_button(void);
void enable_redo_button(void);
void disable_redo_button(void);

#endif /* IO__HEADERS_H */
