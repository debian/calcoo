/*
 * Calcoo: gtkaux_headers.h
 *
 * Copyright (C) 2001 Alexei Kaminski
 *
 */

#ifndef GTKAUX_HEADERS_H
#define GTKAUX_HEADERS_H

void create_n_put_pixmap(GtkWidget **, int, int, char **);
void create_fg_pixmap(GtkWidget **, char **);
void request_selection(GtkWidget *, gpointer);
void get_selection(GtkWidget *, GtkSelectionData *, gpointer);

void own_selection( GtkWidget *, gint * );
gboolean selection_clear( GtkWidget *, GdkEventSelection *, gint *);
void set_selection(GtkWidget * , GtkSelectionData *, guint, guint, gpointer);

#endif /* GTKAUX_HEADERS_H */
