/*
 * Calcoo: b_headers.h
 *
 * Copyright (C) 2001,2002 Alexei Kaminski
 *
 * headers for the functions defined in the files b_foo.c 
 */

#ifndef BODY_HEADERS_H
#define BODY_HEADERS_H

/* b_layout.c */
void create_widgets(void);

/* b_main.c */ 
void create_body(void);
void malloc_body(void);
void create_main_window(int, int);
void set_rpn_mode(int);
int  get_rpn_mode(void);
void set_enter_mode(int);
int  get_enter_mode(void);
void set_rounding_mode(int);
int  get_rounding_mode(void);
int get_trunc_zeros_mode(void);
void set_trunc_zeros_mode(int);
void set_stack_mode(int);
int  get_stack_mode(void);
int get_angle_units(void);
void set_angle_units(int);
void call_exit(void);

/* b_buttons.c */ 
void create_calcoo_display(int, int, int, int);
void create_button(int, int, int, int, int, char ***, void (*)(void), char *);
/* void create_invisible_button(int, void (*)(void)); */
void create_toggle_button(int, int, int, int, int, char ***, void (*)(void), 
			  char *);
void create_copy_button(int, int, int, int, int, char ***, char *);
void create_paste_button(int, int, int, int, int, char ***, char *);
void toggled_arc_button(void);
void toggled_hyp_button(void);
int  get_toggle_button_state(int);
void raise_toggle_button(int);
void raise_arc_hyp(void);
int  get_arc_state(void);
int  get_hyp_state(void);
void set_arc_autorelease(int);
void set_hyp_autorelease(int);
int  get_arc_autorelease(void);
int  get_hyp_autorelease(void);
void enable_button(int);
void disable_button(int);
void hide_button(int);
void show_button(int);
void hide_button_label(int);
void show_button_label(int);
void disable_all_buttons(void);
void enable_all_buttons(void);

/* b_display.c */
void malloc_displays(void);

void create_display(int,
		    int, int, int, int,
		    int, int, int, int, int, int, int, int,
		    int,
		    char ***);
void clear_display(int);
void show_int_digit(int, int, int);
void show_frac_digit(int, int, int);
void show_exp_digit(int, int, int);
void show_minus(int, int);
void show_int_tick(int, int);
void show_frac_tick(int, int);
void show_E_and_exp_sign(int, int);
void show_dot(int, int);
void show_error_message(int);
void create_simple_display(int, int, int, int, int, int, int, char ***);
void clear_simple_display(int);
void show_display_glyph(int, int);
void set_char_display(char *);

/* b_pixmaps.c */
void assign_pixmaps(char ***, char ***, char ***, char ***, 
		    char ***, char ***, char ***);

/* b_info.c */
void call_info (void);
void output_text(char **);
void process_options(char*);

/* b_settings.c */
void call_settings (void);

/* b_loadsave.c */
void load_settings(void);
void save_settings(void);



/* main.c */

#endif /* BODY_HEADERS_H */
