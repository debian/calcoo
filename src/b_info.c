/*
 * Calcoo: b_info.c
 *
 * Copyright (C) 2001, 2005, 2007 Alexei Kaminski
 *
 * creates and operates the info window
 *
 * some code used in this file is taken from about.c of
 * XMMS - Cross-platform multimedia player
 * Copyright (C) 1998-2000  Peter Alm, Mikael Alm, Olle Hallnas, 
 * Thomas Nilsson, and 4Front Technologies
 * Copyright (C) 2000 Havard Kvalen <havardk@xmms.org>
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <string.h>

#include "body.h"
#include "b_headers.h"
#include "texts.h"

#include "pixmaps/main.xpm"

#define INFO_SCROLLWIN_HEIGHT 250
#define INFO_SPACING 10
#define INFO_LINE_SPACING 5

GtkWidget *close_button;

gboolean info_key_press_handler(GtkWidget*, GdkEventKey*, gpointer);

void call_info (void)
{
	static GtkWidget *info_window = NULL;

	GtkWidget *info_vbox;
	GtkWidget *info_notebook;
	GtkWidget *button_box;

	GtkWidget *about_vbox, *about_label;

	GtkWidget *license_vbox, *license_textview, *license_scrollwin;
	GtkWidget *shortcuts_vbox, *shortcuts_scrollwin;
	GtkListStore *shortcuts_store;
	GtkTreeIter  shortcuts_iter;
	GtkWidget *shortcuts_tree;
	GtkCellRenderer *shortcuts_renderer_right_align, *shortcuts_renderer_left_align;
	GtkTreeViewColumn *shortcuts_column;

	GtkWidget *help_vbox, *help_textview, *help_scrollwin;
	GtkTextBuffer *license_buf, *help_buf;

	gint i;
	char *shortcuts_header[3];

	if (info_window)
		return;
	
/* Main info window */
	info_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(info_window), "About Calcoo");
	gtk_window_set_resizable(GTK_WINDOW(info_window),TRUE);
	gtk_container_set_border_width(GTK_CONTAINER(info_window), 
				       INFO_SPACING);
	g_signal_connect(G_OBJECT(info_window), "destroy",
			   GTK_SIGNAL_FUNC(gtk_widget_destroyed), 
			   &info_window);
	g_signal_connect (G_OBJECT (info_window), 
			  "key_press_event",
			  G_CALLBACK (info_key_press_handler), 
			  NULL);
	/* this is to have Escape close the window. Creating an accelerator
	 * group does not work, because the accelerator is not called when
	 * a page with a scrollable text (like license) is open */

	gtk_widget_realize(info_window);
	
/* Main info window icon */

  	gtk_window_set_icon (GTK_WINDOW(info_window),  
  			gdk_pixbuf_new_from_xpm_data((const char **)main_xpm));


	info_vbox = gtk_vbox_new(FALSE, INFO_SPACING);
	gtk_container_add(GTK_CONTAINER(info_window), info_vbox);
	
	info_notebook = gtk_notebook_new();
	gtk_box_pack_start(GTK_BOX(info_vbox), info_notebook, TRUE, TRUE, 0);
	

	/* About */
	about_vbox = gtk_vbox_new(TRUE, INFO_LINE_SPACING);
	gtk_container_set_border_width(GTK_CONTAINER(about_vbox), 
				       INFO_SPACING);
	
  	i = 0; 
  	while (about_text[i] || about_text[i + 1]) { 
		about_label = gtk_label_new(about_text[i]);
		gtk_box_pack_start(GTK_BOX(about_vbox), about_label, 
				   FALSE, FALSE, 0);
		
		i++;
	}

	gtk_notebook_append_page(GTK_NOTEBOOK(info_notebook), 
				 about_vbox,
				 gtk_label_new("About"));
	
	/* License */
	license_buf = gtk_text_buffer_new(NULL);
	gtk_text_buffer_set_text(license_buf, license_text, -1);
	license_textview = gtk_text_view_new_with_buffer(license_buf);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(license_textview), GTK_WRAP_WORD);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(license_textview), FALSE);

	license_scrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(license_scrollwin),
				       GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(license_scrollwin),
					    GTK_SHADOW_IN);
	gtk_container_add(GTK_CONTAINER(license_scrollwin), license_textview);


	license_vbox = gtk_vbox_new(FALSE, INFO_SPACING);
	gtk_container_set_border_width(GTK_CONTAINER(license_vbox), 
				       INFO_SPACING);
  	gtk_box_pack_start(GTK_BOX(license_vbox), license_scrollwin, 
			   TRUE, TRUE, 0); 
  	gtk_widget_set_size_request(license_scrollwin, -1, INFO_SCROLLWIN_HEIGHT); 


	gtk_notebook_append_page(GTK_NOTEBOOK(info_notebook), 
				 license_vbox,
				 gtk_label_new("License"));

	/* Shortcuts */

	shortcuts_header[0] = "Shortcuts";
	shortcuts_header[1] = " ";
	shortcuts_header[2] = "Actions";
	shortcuts_store = gtk_list_store_new ( 3,
					       G_TYPE_STRING,
					       G_TYPE_STRING,
					       G_TYPE_STRING);

	shortcuts_tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL (shortcuts_store));	

	shortcuts_renderer_right_align = gtk_cell_renderer_text_new();
	gtk_object_set( GTK_OBJECT(shortcuts_renderer_right_align), 
			"xalign", 1.0, 
			NULL );

	shortcuts_renderer_left_align = gtk_cell_renderer_text_new();

	shortcuts_column = gtk_tree_view_column_new_with_attributes (shortcuts_header[0],
								     shortcuts_renderer_right_align,
								     "text", 0,
								     NULL);
	gtk_tree_view_column_set_alignment(shortcuts_column, 1.0);
	gtk_tree_view_append_column (GTK_TREE_VIEW (shortcuts_tree), shortcuts_column);
	shortcuts_column = gtk_tree_view_column_new_with_attributes (shortcuts_header[1],
								     shortcuts_renderer_right_align,
								     "text", 1,
								     NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (shortcuts_tree), shortcuts_column);
	shortcuts_column = gtk_tree_view_column_new_with_attributes (shortcuts_header[2],
								     shortcuts_renderer_left_align,
								     "text", 2,
								     NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (shortcuts_tree), shortcuts_column);


  	i = 0; 
  	while ( shortcuts_text[i] || shortcuts_text[i + 2]) { 
		gtk_list_store_append (shortcuts_store, &shortcuts_iter); 

		gtk_list_store_set (shortcuts_store, &shortcuts_iter,
				    0, shortcuts_text[i],
				    1, "   ",
				    2, shortcuts_text[i+1],
				    -1);
		i++; i++;
	}



	shortcuts_scrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(
		GTK_SCROLLED_WINDOW(shortcuts_scrollwin),
		GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(shortcuts_scrollwin),
					    GTK_SHADOW_IN);
	gtk_container_add(GTK_CONTAINER(shortcuts_scrollwin), shortcuts_tree);

	shortcuts_vbox = gtk_vbox_new(FALSE, INFO_SPACING);
	gtk_container_set_border_width(GTK_CONTAINER(shortcuts_vbox), 
				       INFO_SPACING);
  	gtk_box_pack_start(GTK_BOX(shortcuts_vbox), shortcuts_scrollwin, 
			   TRUE, TRUE, 0); 
  	gtk_widget_set_size_request(shortcuts_scrollwin, -1, INFO_SCROLLWIN_HEIGHT); 


	gtk_notebook_append_page(GTK_NOTEBOOK(info_notebook), 
				 shortcuts_vbox,
				 gtk_label_new("Shortcuts"));

	/* Help */
	help_buf = gtk_text_buffer_new(NULL);
	gtk_text_buffer_set_text(help_buf, help_text, -1);
	help_textview = gtk_text_view_new_with_buffer(help_buf);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(help_textview), GTK_WRAP_WORD);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(help_textview), FALSE);


	help_scrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(help_scrollwin),
				       GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(help_scrollwin),
					    GTK_SHADOW_IN);
	gtk_container_add(GTK_CONTAINER(help_scrollwin), help_textview);


	help_vbox = gtk_vbox_new(FALSE, INFO_SPACING);
	gtk_container_set_border_width(GTK_CONTAINER(help_vbox), 
				       INFO_SPACING);
  	gtk_box_pack_start(GTK_BOX(help_vbox), help_scrollwin, 
			   TRUE, TRUE, 0); 
  	gtk_widget_set_size_request(help_scrollwin, -1, INFO_SCROLLWIN_HEIGHT); 

	gtk_notebook_append_page(GTK_NOTEBOOK(info_notebook), 
				 help_vbox,
				 gtk_label_new("Help"));

	/* "Close" button */
	button_box = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(button_box), 
				  GTK_BUTTONBOX_END);
	gtk_box_set_spacing(GTK_BOX(button_box), INFO_SPACING);
	gtk_box_pack_start(GTK_BOX(info_vbox), button_box, FALSE, FALSE, 0);

	close_button = gtk_button_new_with_label("Close");
	g_signal_connect_swapped(G_OBJECT(close_button), "clicked",
				  GTK_SIGNAL_FUNC(gtk_widget_destroy),
				  G_OBJECT(info_window));
	gtk_box_pack_start(GTK_BOX(button_box), close_button, TRUE, TRUE, 0);

	gtk_widget_show_all(info_window);
}

void process_options(char *argv1)
{
	if ( !strcmp(argv1,"-v") || !strcmp(argv1,"--version"))
		output_text(version_console);
	else if ( !strcmp(argv1,"-h") || !strcmp(argv1,"--help"))
		output_text(help_console);
	else 
		output_text(invalid_console);
}

void output_text(char **t)
{
	int i;
	for ( i = 0; t[i] != NULL; i++ )
		printf("%s\n",t[i]);
}

gboolean info_key_press_handler(GtkWidget* widget, 
				GdkEventKey* event, 
				gpointer data)
{
  switch (event->keyval)
    {
    case GDK_Escape:
	    g_signal_emit_by_name(G_OBJECT(close_button),
				    "clicked");
	    return TRUE;
    default:
	    /* passing for further processing */
	    return FALSE;
    }
}

