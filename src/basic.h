/*
 * Calcoo: basic.h
 *
 * Copyright (C) 2001 Alexei Kaminski
 *
 */

#ifndef BASIC_H
#define BASIC_H

#define INPUT_LENGTH 10
#define EXP_INPUT_LENGTH 2

/* this definition affects only the body. For the cpu,
 * the base is hard-coded, sorry */
#define BASE 10

#define NUMBER_OF_MEMS 2

/* the maximum number of glyphs in a simple display
 * the simple displays are:
 * - the displays for the operations in the stack 6 glyphs (5 ops + paren)
 * - the deg/rad indicator (2 glyphs)
 * - the exp/f.p. indicator */
#define SD_G_MAX 6

#define MAX_BUTTON_NUMBER 256

#endif /* BASIC_H */

