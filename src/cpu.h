/*
 * Calcoo: cpu.h
 *
 * Copyright (C) 2001,2002 Alexei Kaminski
 *
 */

#ifndef CPU_H
#define CPU_H

#include "basic.h"
#include "displays.h"

#define INPUT_FIELD_INT 1
#define INPUT_FIELD_FRAC 2
#define INPUT_FIELD_EXP 3

typedef struct tse {
	double z;
	int op;
	int number_of_parens ;
	struct tse *next;
} t_stack_element;

/* Attention! This structures are being copied during pushing and popping
 * of the undo stack. All changes in the strucure t_calcoo_cpu must be
 * accompanied by proper modifications of copy_cpu() function in cpu_undo.c.
 * Also all substructures of t_calcoo_cpu that need to be malloc'ed must
 * be also malloc'ed in init_undo_stack() in cpu_undo.c 
 * Also don't forget cpu initalization in c_main.c! */
typedef struct tcd {
	int input_field, sign, exp_sign;
	int int_field[INPUT_LENGTH];
	int frac_field[INPUT_LENGTH]; 
	int exp_field[EXP_INPUT_LENGTH];
	int n_int,n_frac;
	int format;
	int display_overflow;
} t_cpu_display ;

typedef struct tcod {
	int op_code;
	int show_brace;
} t_cpu_operation_display ;


typedef struct t_c_s {
	double x, y, z, t, mem[NUMBER_OF_MEMS];
	int    op, number_of_parens;
	t_stack_element *stack_head;
	t_cpu_display *d, *mem_d[NUMBER_OF_MEMS], 
		      *reg_d[NUMBER_OF_REG_DISPLAYS];
	t_cpu_operation_display *op_d[NUMBER_OF_REG_DISPLAYS];
	int    last_action;
	double precision;
	int    x_overflow;
	int    prescribed_format;
	int    rounding;
	int    trunc_zeros;
	int    digits_to_keep;
	int    curr_mem;
	int    error;

	/* the following are set by the loaded options and need not to be 
	 * initialized or saved in undo */
	int    rpn_mode, enter_mode, stack_mode, angle_units;
	/* arc_ and hyp_autorelease state is stored in the body, see body.h */
} t_calcoo_cpu ;

extern t_calcoo_cpu *cpu;
/* the variable cpu itself is defined in c_main.c */

#endif /* CPU_H */

 
