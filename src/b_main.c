/*
 * Calcoo: b_main.c
 *
 * Copyright (C) 2001-2005 Alexei Kaminski
 *
 */

#include <stdlib.h>
#include <gtk/gtk.h>

#include "codes.h"
#include "body.h"
#include "defaults.h"
#include "b_headers.h"
#include "gtkaux_headers.h"
#include "io_headers.h"
#include "aux_headers.h"
#include "b_accel_headers.h"

#include "pixmaps/main.xpm"

t_calcoo_body *body;


void create_body(void)
{
	malloc_body();
	malloc_displays(); /* these displays are not widgets, but structures to
			    * store information what to display in the
			    * corresponding widgets */
	create_widgets();
}

void malloc_body(void)
{
	body = (t_calcoo_body*) malloc(sizeof(t_calcoo_body));
}


void create_main_window(int w, int h)
{
	//GdkPixmap *main_pixmap;
	//GdkBitmap *main_mask;
	int i;

/* Main widget */
	body->main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (body->main_window), "Calcoo");
	gtk_container_set_border_width (GTK_CONTAINER (body->main_window), 0);
	gtk_widget_set_size_request(body->main_window, w, h);
	gtk_window_set_resizable (GTK_WINDOW (body->main_window), 
			       FALSE);

	g_signal_connect (G_OBJECT (body->main_window), 
			    "delete_event",
			    GTK_SIGNAL_FUNC (gtk_main_quit), 
			    NULL);
	g_signal_connect (G_OBJECT (body->main_window), 
			    "destroy",
			    GTK_SIGNAL_FUNC (gtk_main_quit), 
			    NULL);
	g_signal_connect (G_OBJECT (body->main_window), 
			    "key_press_event",
			    GTK_SIGNAL_FUNC (key_press_handler), 
			    NULL);
	g_signal_connect (G_OBJECT (body->main_window), 
			    "button_press_event",
			    GTK_SIGNAL_FUNC (button_press_handler), 
			    NULL);
	gtk_widget_set_events (body->main_window, 
			       GDK_BUTTON_PRESS_MASK );

	gtk_widget_show (body->main_window);

	body->style = gtk_style_copy (
		gtk_widget_get_style (body->main_window) 
		);

/* Main window icon */
/*
	main_pixmap = gdk_pixmap_create_from_xpm_d ( 
		(body->main_window)->window,
		&main_mask,
		&(body->style)->bg[GTK_STATE_NORMAL],
		(gchar **) main_xpm 
		);

  	gdk_window_set_icon (body->main_window->window, NULL,  
  			     main_pixmap, main_mask);
*/
  	gtk_window_set_icon (GTK_WINDOW(body->main_window),  
  			gdk_pixbuf_new_from_xpm_data((const char **)main_xpm));

/* Fixer to fix the button positions and sizes */
	body->fixer = gtk_fixed_new();
	gtk_container_add (GTK_CONTAINER(body->main_window), body->fixer);
	gtk_widget_show (body->fixer);

/* Button tooltips */
	body->button_tooltips = gtk_tooltips_new();
	//gtk_tooltips_set_delay(body->button_tooltips, DEFAULT_TOOLTIP_DELAY);

/* Setting all button pointers to NULL in order to be able to tell
 * existing from non-existing buttons when making all the buttons 
 * active/non-active, like after an error */
	for (i = 0; i < MAX_BUTTON_NUMBER; i++)
		body->button[i] = NULL;

	gtk_widget_show (body->main_window);

}

void set_rpn_mode(int a)
{
	if (a) {
		show_button(CODE_ENTER);
		show_button(CODE_STACK_DOWN);
		show_button(CODE_STACK_UP);
		hide_button(CODE_EQ);
		hide_button(CODE_RIGHT_PAREN);
		hide_button(CODE_LEFT_PAREN);
	} else {
		show_button(CODE_EQ);
		show_button(CODE_RIGHT_PAREN);
		show_button(CODE_LEFT_PAREN);
		hide_button(CODE_ENTER);
		hide_button(CODE_STACK_DOWN);
		hide_button(CODE_STACK_UP);
	}
	clicked_set_rpn_mode(a);
}

/* One may wonder why we introduce all these get_ and set_ functions below, 
 * which do not do anything but passing the requests further. The answer is 
 * that they pass it directly to the cpu, and we may still need to do something
 * in the body. It is the case for set_rpn_mode(). It is not the case for the 
 * other functions, but since it may be the case in the future and also for 
 * uniformity, we decided to have these functions. */

int get_rpn_mode(void)
{
	return requested_rpn_mode();
}

void set_enter_mode(int a)
{
	clicked_set_enter_mode(a);
}

int get_rounding_mode(void)
{
	return requested_rounding_mode();
}

void set_rounding_mode(int a)
{
	clicked_set_rounding_mode(a);
}

int get_trunc_zeros_mode(void)
{
	return requested_trunc_zeros_mode();
} 

void set_trunc_zeros_mode(int a)
{
	clicked_set_trunc_zeros_mode(a);
}

int get_enter_mode(void)
{
	return requested_enter_mode();
}

void set_stack_mode(int a)
{
	clicked_set_stack_mode(a);
}

int get_stack_mode(void)
{
	return requested_stack_mode();
}

int get_angle_units(void)
{
	return requested_angle_units();
}

void set_angle_units(int a)
{
	clicked_set_angle_units(a);
}

void call_exit(void)
{
	gtk_main_quit();
}

