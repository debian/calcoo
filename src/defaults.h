/*
 * Calcoo: defaults.h
 *
 * Copyright (C) 2001, 2002, 2003 Alexei Kaminski
 *
 */

#ifndef DEFAULTS_H
#define DEFAULTS_H

#include "codes.h"
#include "const.h"

#define DEFAULT_TOOLTIP_DELAY 1500
/* in milliseconds */

#define UNDO_STACK_SIZE 64

#define DEFAULT_DISPLAY_FORMAT FORMAT_FIX
/* the other possible choices are FORMAT_EXP and FORMAT_ENG */


/****************************************************
 * inner precision; used in init_cpu() in c_main.c  *
 ****************************************************/

#define MAXIMIZE_PRECISION TRUE


/* ***********************************************
 * the following options are overridden with     *
 * the settings found in $HOME/.calcoo file      *
 *************************************************/

#define DEFAULT_ANGLE_UNITS CODE_DEG
/* possible choices: CODE_RAD and CODE_DEG */

#define DEFAULT_RPN_MODE FALSE

#define DEFAULT_ENTER_MODE ENTER_MODE_TRADITIONAL
/* possible choices: ENTER_MODE_TRADITIONAL and ENTER_MODE_HP28 */

#define DEFAULT_STACK_MODE STACK_MODE_XYZT
/* possible choices: STACK_MODE_XYZT and STACK_MODE_INFINITE */

#define DEFAULT_ARC_AUTORELEASE TRUE
#define DEFAULT_HYP_AUTORELEASE TRUE

#define DEFAULT_TRUNC_ZEROS_MODE TRUE


/**************************************************
 *  options dealing with the default color        *
 *  for button icons and display glyphs           *
 **************************************************/

#define CONVERT_ICONS_TO_FOREGROUND_COLOR TRUE
/* is FALSE, the original pixmap colors will be preserved */

#define GTK_STATE_FOR_CALCOO_FOREGROUND_COLOR GTK_STATE_PRELIGHT
/* Some other sensible choices are GTK_STATE_NORMAL, GTK_STATE_ACTIVE,
 * GTK_STATE_SELECTED. Actually it strongly depends on the GTK
 * color scheme you are using, so the choice is yours */


/********************************************************
 *  debugging                                           *
 * CALCOO_DEBUG is defined through the configure script *
 ********************************************************/

#ifdef CALCOO_DEBUG
#define REPORT_NONCRITICAL_ERRORS TRUE
#define DEBUG_MESSAGES_ON TRUE /* the ones output by mess() and messd() */

#else 

#define REPORT_NONCRITICAL_ERRORS FALSE
#define DEBUG_MESSAGES_ON FALSE /* the ones output by mess() and messd() */

#endif 

#endif /* DEFAULTS_H */
