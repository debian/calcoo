/*
 * Calcoo: c_mem.c
 *
 * Copyright (C) 2001, 2002 Alexei Kaminski
 *
 * memory operations
 *
 */

#include "const.h"
#include "codes.h"
#include "cpu.h"
#include "c_headers.h"
#include "io_headers.h"
#include "aux_headers.h"

void call_mem_op(int received_code)
{
	double tmp;

	if (cpu->last_action == ACTION_INPUT) 
		input_to_x();

	switch (received_code){
	case CODE_MEM_PLUS  :  
		cpu->mem[cpu->curr_mem] = smart_sum(cpu->x, 
						    cpu->mem[cpu->curr_mem],
						    cpu->precision);
		break;
	case CODE_MEM_TO_X  :  
		if (cpu->rpn_mode) {
			push_stack(); 
			cpu->op = CODE_ENTER; /* should be removed (?) */
			cpu->y = cpu->x;
		}
		cpu->x = cpu->mem[cpu->curr_mem];
		break;
	case CODE_X_TO_MEM :  
		cpu->mem[cpu->curr_mem] = cpu->x;
		break;
	case CODE_EXCH_XMEM :  
		tmp = cpu->x;
		cpu->x = cpu->mem[cpu->curr_mem];
		cpu->mem[cpu->curr_mem] = tmp;
		break;
	default:
		error_occured("call_mem_op() unknown mem_op code", FALSE);
  }

	cpu->last_action = ACTION_ENTER;

	aftermath();
}

void call_switch_to_mem(int m)
{
	if ( (m >= 0) && (m < NUMBER_OF_MEMS)) { /* sanity check */
		cpu->curr_mem = m;

		save_for_undo();
		refresh_body();
	} else
		error_occured("call_switch_to_mem() no such memory register",
			      FALSE);

}
