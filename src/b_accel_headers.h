/*
 * Calcoo: b_accel_headers.h
 *
 * Copyright (C) 2005 Alexei Kaminski
 *
 * headers for the functions defined in the files b_accel.c 
 * separate header file because we do not want GTK stuff in b_headers.h
 */

/* b_accel.c */
void key_press_handler(GtkWidget*, GdkEventKey*, gpointer);
gboolean button_press_handler(GtkWidget*, GdkEventButton*);
