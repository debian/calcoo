/*
 * Calcoo: b_buttons.c
 *
 * Copyright (C) 2001, 2002, 2005 Alexei Kaminski
 *
 * functions to create and operate buttons
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "codes.h"
#include "body.h"
#include "defaults.h"
#include "b_headers.h"
#include "gtkaux_headers.h"
#include "io_headers.h"

void create_button(int button_code,
		   int x, int y, int w, int h,
		   char ***icon_xpm,
		   void (*button_function)(void),
		   char *tooltip)
{
	create_fg_pixmap(&(body->icon[button_code]), icon_xpm[button_code]);
	gtk_widget_show(body->icon[button_code]);

	body->button[button_code] = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(body->button[button_code]), 
			  body->icon[button_code]);
	g_signal_connect(G_OBJECT (body->button[button_code]), "clicked",
			   GTK_SIGNAL_FUNC (*button_function), NULL);
	gtk_fixed_put(GTK_FIXED(body->fixer), body->button[button_code], x, y);
	GTK_WIDGET_UNSET_FLAGS(body->button[button_code], GTK_CAN_FOCUS);
	gtk_widget_set_size_request(body->button[button_code], w, h);
	gtk_widget_show(body->button[button_code]);

	if (tooltip)
		gtk_tooltips_set_tip(GTK_TOOLTIPS (body->button_tooltips), 
				     body->button[button_code], tooltip, NULL);
}

/* Invisible buttons are fake buttons necessary (?) to have keyboard
 * accelerators for functions which are not (and should not be) tied to any 
 * specific real button. Exit is an example of such a function */
/* void create_invisible_button(int button_code, */
/* 		   void (*button_function)(void)) */
/* { */
/* 	body->button[button_code] = gtk_button_new(); */
/* 	gtk_signal_connect(GTK_OBJECT (body->button[button_code]), "clicked", */
/* 			   GTK_SIGNAL_FUNC (*button_function), NULL); */
/* 	gtk_fixed_put(GTK_FIXED(body->fixer), body->button[button_code], 0, 0); */
/* 	GTK_WIDGET_UNSET_FLAGS(body->button[button_code], GTK_CAN_FOCUS); */
/* 	gtk_widget_set_usize(body->button[button_code], 0, 0); */
/* } */

void create_toggle_button(int button_code,
			  int x, int y, int w, int h,
			  char ***icon_xpm,
			  void (*button_function)(void),
			  char *tooltip)
{
	create_fg_pixmap(&(body->icon[button_code]), icon_xpm[button_code]);
	gtk_widget_show(body->icon[button_code]);

	body->button[button_code] = gtk_toggle_button_new();
	gtk_container_add(GTK_CONTAINER(body->button[button_code]), 
			  body->icon[button_code]);
	g_signal_connect(G_OBJECT (body->button[button_code]), "toggled",
			   GTK_SIGNAL_FUNC (*button_function), NULL);
	gtk_fixed_put(GTK_FIXED(body->fixer), body->button[button_code], x, y);
	GTK_WIDGET_UNSET_FLAGS(body->button[button_code], GTK_CAN_FOCUS);
	gtk_widget_set_size_request(body->button[button_code], w, h);
	gtk_widget_show(body->button[button_code]);

	if (tooltip)
		gtk_tooltips_set_tip(GTK_TOOLTIPS (body->button_tooltips), 
				     body->button[button_code], tooltip, NULL);
}

void create_copy_button(int button_code,
			int x, int y, int w, int h,
			char ***icon_xpm,
			char *tooltip)
/* a special function is needed to create the copy button because 
 * an additional signal connection is needed for it (see below) */
{
	static int have_selection = FALSE;

	body->selection_widget = gtk_invisible_new ();

 	create_fg_pixmap(&(body->icon[button_code]), icon_xpm[button_code]);
	gtk_widget_show(body->icon[button_code]);

	body->button[button_code] = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(body->button[button_code]), 
			  body->icon[button_code]);

	/* now let us create copying process */
	g_signal_connect (G_OBJECT (body->button[button_code]), 
			  "clicked",
			  G_CALLBACK (own_selection), 
			  (gpointer) &have_selection
		);
	g_signal_connect (G_OBJECT (body->selection_widget),
			  "selection_clear_event",
			  G_CALLBACK (selection_clear),
			  (gpointer) &have_selection
		);
	gtk_selection_add_target (body->selection_widget,
				  GDK_SELECTION_PRIMARY,
				  GDK_SELECTION_TYPE_STRING,
				  1);
	g_signal_connect (G_OBJECT (body->selection_widget), 
			  "selection_get",
			  G_CALLBACK (set_selection), 
			  (gpointer) &have_selection
		);

	gtk_fixed_put(GTK_FIXED(body->fixer), body->button[button_code], x, y);
	GTK_WIDGET_UNSET_FLAGS(body->button[button_code], GTK_CAN_FOCUS);
	gtk_widget_set_size_request(body->button[button_code], w, h);
	gtk_widget_show(body->button[button_code]);

	if (tooltip)
		gtk_tooltips_set_tip(GTK_TOOLTIPS (body->button_tooltips), 
				     body->button[button_code], tooltip, NULL);
}


void create_paste_button(int button_code,
			int x, int y, int w, int h,
			char ***icon_xpm,
			char *tooltip)
/* a special function is needed to create the paste button because 
 * an additional signal connection is needed for it (see below) */
{
	create_fg_pixmap(&(body->icon[button_code]), icon_xpm[button_code]);
	gtk_widget_show(body->icon[button_code]);

	body->button[button_code] = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(body->button[button_code]), 
			  body->icon[button_code]);

	g_signal_connect (G_OBJECT (body->button[button_code]), 
			  "clicked",
			  G_CALLBACK (request_selection), 
			  (gpointer)body->main_window
		);
	g_signal_connect (G_OBJECT(body->main_window), 
			  "selection_received",
			  G_CALLBACK (get_selection), NULL);

	gtk_fixed_put(GTK_FIXED(body->fixer), body->button[button_code], x, y);
	GTK_WIDGET_UNSET_FLAGS(body->button[button_code], GTK_CAN_FOCUS);
	gtk_widget_set_size_request(body->button[button_code], w, h);
	gtk_widget_show(body->button[button_code]);

	if (tooltip)
		gtk_tooltips_set_tip(GTK_TOOLTIPS (body->button_tooltips), 
				     body->button[button_code], tooltip, NULL);
}

void show_button_label(int button_code)
{
	gtk_widget_show(body->icon[button_code]);
}

void hide_button_label(int button_code)
{
	gtk_widget_hide(body->icon[button_code]);
}

void toggled_arc_button(void)
{
/* this empty function is kept just in case,
 * but in GTK+ 1.0 it was necessary since there were
 * no function to determine the state of a toggle button,
 * and this function set the corresponding auxiliary flag */
}

void toggled_hyp_button(void)
{
/* this empty function is kept just in case,
 * but in GTK+ 1.0 it was necessary since there were
 * no function to determine the state of a toggle button,
 * and this function set the corresponding auxiliary flag */
}

void raise_toggle_button(int button_code)
{
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(body->button[button_code]), FALSE);
}

int get_toggle_button_state(int button_code)
{
	return gtk_toggle_button_get_active(
		GTK_TOGGLE_BUTTON(body->button[button_code]));
}

void raise_arc_hyp(void)
{
	if (body->arc_autorelease)
		raise_toggle_button(CODE_ARC);
	if (body->hyp_autorelease)
		raise_toggle_button(CODE_HYP);
}

int get_arc_state(void)
{
	return get_toggle_button_state(CODE_ARC);
}

int get_hyp_state(void)
{
	return get_toggle_button_state(CODE_HYP);
}

void set_arc_autorelease(int a)
{
	body->arc_autorelease = a;
}

void set_hyp_autorelease(int a)
{
	body->hyp_autorelease = a;
}

int get_arc_autorelease(void)
{
	return body->arc_autorelease;
}

int get_hyp_autorelease(void)
{
	return body->hyp_autorelease;
}

void enable_button(int button_code)
{
	gtk_widget_set_sensitive(body->button[button_code],TRUE);
}

void disable_button(int button_code)
{
	gtk_widget_set_sensitive(body->button[button_code],FALSE);
}

void show_button(int button_code)
{
	gtk_widget_show(body->button[button_code]);
}

void hide_button(int button_code)
{
	gtk_widget_hide(body->button[button_code]);
}

void disable_all_buttons(void)
{
	int i;

	for (i = 0; i < MAX_BUTTON_NUMBER; i++) {
		if ((body->button[i] != NULL)
		    && (i != CODE_CLEAR_ALL)
		    && (i != CODE_UNDO)
		    && (i != CODE_REDO))
			disable_button(i);
	}

}

void enable_all_buttons(void)
{
	int i;

	for (i = 0; i < MAX_BUTTON_NUMBER; i++) {
		if ((body->button[i] != NULL)
		    && (i != CODE_CLEAR_ALL)
		    && (i != CODE_UNDO)
 		    && (i != CODE_REDO))
			enable_button(i);
	}

}
